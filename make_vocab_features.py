#!/usr/bin/env python

import sys

USAGE="python %s vocabfilename corpusfilename" % sys.argv[0]

'''
vocabfilename is a file where each line is "wID count word",
with each wID being a unique positive integer (as in,
unique to the word and appearing only once in that column of
the file).
corpusfilename is a file where each line is "userID wID count",
with userIDs assigned uniquely to users, and each word ID having
exactly one appearance for each user.
See the documentation to preprocessing.py for more info.

We want to make a new tab-delimited file where the
u-th line is a binary feature vector whose w-th entry is
1 if word w appeared in document u, and 0 otherwise.
The ID w corresponds to word on the w-th line of
vocabfilename.
'''

if len(sys.argv) != 3:
	print USAGE
	sys.exit()

vocabfilename = sys.argv[1]
corpusfilename = sys.argv[2]

# populate the vocabulary.
vocfh = open(vocabfilename, 'r')

'''vocabulary maps each word ID to the line
number on which it appeared.'''
vocabulary = dict()
lineNumber = 0
for line in vocfh:
	(wID,count,text) = line.split()
	vocabulary[int(wID)] = lineNumber
	lineNumber += 1
vocfh.close()

'''Each userID is mapped to a list of all the words
that appeared in the given vocab list and appeared
at least once in that user's statuses'''
vocabByUser = dict()

corfh = open(corpusfilename, 'r')
for line in corfh:
	(uID, wID, count) = line.split()
	uID = int(uID)
	wID = int(wID)
	if uID not in vocabByUser:
		vocabByUser[uID] = list()
	if wID in vocabulary:
		vocabByUser[uID].append(wID)
corfh.close()

'''For each user, we want to write out a binary
feature vector where the w-th entry is 1 if word w
(indexed from vocabulary list) appeared at least
once in that user's document, and is 0 otherwise.
'''
users = vocabByUser.keys()
users.sort()
for u in users:
	featvec = ['0']*len(vocabulary)
	for w in vocabByUser[u]:
		if w in vocabulary:
			lineNumber = vocabulary[w]
			featvec[lineNumber] = '1'
	print "%s" % '\t'.join(featvec)
