\documentclass[11pt]{article}

\usepackage{amsmath}
\usepackage{enumerate}
\usepackage{bm}

\oddsidemargin 0mm
\evensidemargin 5mm
\topmargin -20mm
\textheight 240mm
\textwidth 160mm


\pagestyle{myheadings}
\markboth{EN 600.676}{}

\title{Appendix to Final Project\\}
\author{Olivia Buzek and Keith Levin}

\begin{document}
\large
\maketitle
\thispagestyle{headings}

\section{Logistic Regression}
We performed regression to predict traits based on word and topic
occurrence (in our baseline logistic regression and LDA
logistic regression model, respectively).
Given a vector of traits $Y$ where each row is a
trait vector for some user and a matrix $X$ where each row corresponds
to a word occurence vector ($X_{ij}=1$ if user $i$ used word $j$ at
least once, $X_{ij}=0$ otherwise), we wish to find the vector of
weights $w$ that minimizes $ J(w) =\| y - \sigma(Xw) \|^2_2 ,$ where
$\sigma(z)$ is the logistic function
$$ \sigma(z) = \frac{1}{1+\exp\{-z\}} .$$

\paragraph{}
In the case where $y$ is binary (as in our essays data set),
this is straight-forward logistic regression, where $\eta$ is
the learning rate and the batch gradient ascent update
rule is given by
$$ w^{(t+1)} = w^t + \eta \nabla_w J(w^t) $$
with
$$ \frac{ \partial }{\partial w_j} T(w)
    = \sum_i (y^{(i)} - \sigma(x^{(i) \text{T}}w))x^{(i)}_j $$
where $i$ ranges over the data points. 
\paragraph{}
In the case where $y$ is continuous on $[0,1]$, our problem
amounts to doing actual regression in logistic space. We
again wish to minimize $J(w) = \frac{1}{2} \| y - Xw \|^2_2$,
where we have introduced a scaling factor for mathematical
convenience. Now we can't use the fact that we know $y$
to be binary. Taking the gradient
with respect to the weights, we get
$$ \frac{ \partial }{\partial w_j} J(w)
    = \sum_i (y^{(i)}-\sigma(x^{(i) \text{T}} w)
			(1-\sigma(x^{(i) \text{T}} w)
			(-\sigma(x^{(i) \text{T}} w))x^{(i)}_j .$$
We experimented with several variants of gradient ascent (e.g.,
block and stochastic gradient descent) as well as with
a second-order method. We had hoped that, for example, a
Newton-Raphson method would converge faster or would tend
to end up closer to the true minimum, but no method proved
noticeably more effective than another, so we settled on using batch
gradient descent for simplicity.

\section{Dirichlet Multinomial Regression}
The conditional likelihood in our adapted DMR model is given by
(we have written things here as though there were only one
user for the sake of clarity, since the extension to
more than one user is simple)
\begin{equation*}
p(\vec{x},\vec{w},\vec{z},\theta,\alpha \mid \rho,\Lambda,\sigma)
= p(\vec{x} \mid \rho) p(\Lambda \mid \sigma_\lambda)
    p(\alpha \mid \vec{x},\Lambda,\sigma_\alpha) p(\theta \mid \alpha)
    p(\vec{z} \mid \theta) p(\vec{w} \mid \vec{z},\phi) p(\phi \mid \beta).
\end{equation*}
The entries of $\vec{x}$ are drawn i.i.d. either Bernoulli
(in the case of the essays data) or Beta (in the case of the
Facebook data), parameterized by a vector of parameters
$\rho$. The weights in $\Lambda$ are drawn from a zero-mean
Gaussian distribution with variance $\sigma_\lambda^2$.
$\alpha$ is deterministically equal to $\vec{x}^{\text{T}}\Lambda$.
$\theta$ is drawn from a Dirichlet distribution with parameter
vector $\alpha$. The entries of $\vec{z}$ are
drawn from a categorical distribution parameterized by $\theta$. 
Entries of $\vec{w}$ are drawn from a multinomial parameterized
by $\vec{z}$ and the word distribution matrix $\phi$, which
has each of its rows drawn from a Dirichlet distribution
with parameter vector $\beta$.
\paragraph{}

Our first instinct for deriving a sampler for a problem
like this would be to attempt to marginalize out the
intermediate parameters, just as we would in vanilla LDA,
but we quickly run into trouble. Consider
\begin{align*}
  p(\theta \mid \vec{x},\Lambda)
	&= \int_\alpha p(\theta \mid \alpha)
		p(\alpha \mid \vec{x},\Lambda) d\alpha \\
	&= \int_\alpha \frac{ \prod_k \theta^{\alpha_k - 1} }
				{ B(\alpha) }
			p(\alpha \mid \vec{x},\Lambda \} )
		d \alpha.
\end{align*}
We see immediately that if we are to be able to integrate out both
$\theta$ and $\alpha$, then $p(\alpha \mid \vec{x},\Lambda \})$
will have to be such that this integral has a closed form
solution. Further, we still want $\theta$ to have a Dirichlet
distribution. Thus, we want $p(\alpha \mid \vec{x},\Lambda \})$
to be the conjugate prior to the Dirichlet. This conjugate
prior exists, since the Dirichlet is an exponential family
(see, for example, chapter 3 of {\it The Bayesian Choice}
by Robert Christian),
but its form is rather inscrutable.

\paragraph{}
To avoid this trouble, we decided to back off to a more
EM-esque approach. Our fundamental problem is that we
do not observe traits (which we will refer to as the matrix
$X$) at test time. We do, however, have an estimate for the
parameter controlling the trait distribution and an estimate
for the weights, $\Lambda$. Thus, if we knew the true values
of the traits, we could use our known weights to get
the LDA topic distribution hyperparameter $\alpha$ and
proceed with LDA to estimate the topic distributions.
Similarly, if we knew the LDA topic distribution hyperparameters,
we could estimate the traits, by finding the traits that
solve
$$ \log( \alpha_k ) = x^{\text{T}}\Lambda$$
subject to the constraint that each trait
lies in a range (or, in the case of the binary outcome
essay data, subject to the constraint that each trait
is either, say, 0 or 1), while simultaneously maximizing
the probability of the traits. That is, we would like to
solve (in the case of the continuous-outcome Facebook
data
\begin{align*}
  &\max_{x} p(x \mid \rho)\\
    &\text{ s.t. }
		\log( \alpha_k ) = x^{\text{T}}\Lambda,
		0 \le x_i \le 1 \forall i,
\end{align*}
or, in the case of the binary-outcome essays data,
\begin{align*}
  &\max_{x} p(x \mid \rho)\\
        &\text{s.t.} \log( \alpha_k ) = x^{\text{T}}\Lambda,
                x_i \in \{0,1\} \forall i.
\end{align*}

In the former case, with a Beta distribution on each entry of $x$,
this becomes a non-linear log-convex optimization problem, provided both
hyperparameters of the Beta distribution are greater than unity.
Unfortunately, we could find no obvious gradient ascent or
similar algorithm that would allow us to solve this subject to
the constraints on $x$.
This is likely more due to our lack of background in optimization
than the actual non-existence of a solution to this problem.
In the essays case, while it is a less than ideal approach, the
fact that there are only 32 possible trait vectors for a given
user makes a brute force search for the ``best'' setting of
the $x$ vector a reasonable enough approach.
\paragraph{}
Thus, at test time on the essays data, we execute the
following iterative algorithm (here we are again writing
as though there were only one user in the test set, but
of course in actuality we run this routine to
estimate $\alpha$ for every user):
\begin{enumerate}[1.]
  \item Run LDA. Set
	$\alpha_k = (\frac{1}{2}\alpha_k^{(a)}
			+ \frac{1}{2}\alpha_k^{(b)})
		\sum_k \sum_f \Lambda_{kf} x_f$, where
	$$\alpha_k^{(a)} = \frac{\theta_k}{\sum_k \theta_k}
		$$
	and
	$$\alpha_k^{(b)} = \frac{ \sum_f \Lambda_{kf}x_f }
			{\sum_k \sum_f \Lambda_{kf} x_f}
		$$
  \item Find the optimal setting of the entries of the $x$ vector by
	$$\hat{x} = \max_{x} p(x \mid \rho)
		- \xi \| \alpha - \exp\{x^{\text{T}}\Lambda\} \|_2 $$
	where $\xi$ is a regularization parameter that controls how much we
	care about getting high-probability traits versus how much we
	care about getting an accurate reconstruction of $\alpha$.
\end{enumerate}
This approach is admittedly less graceful than one would like.
Firstly, we were forced to back off to using regularization to encourage
$\exp\{ x^{\text{T}}\Lambda \}$ to approximately replicate $\alpha$,
rather than the exact constraint that we had hoped to use.
Ideally, we would like to at least have a probabilistic interpretation
to the updates we are making. Unfortunately, this is not the case.
We can, however, interpret the first step in the above algorithm
as updating $\alpha$ by keeping its total magnitude constant
while reallocating how much probability $\alpha$ gives to each
topic outcome. That is, we are preserving how strong our prior
encoded by $\alpha$ is, but changing which of the topics we
believe to be more prevalent a priori.

\end{document}
