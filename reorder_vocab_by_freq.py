#!/usr/bin/env python

import sys

vocab_file = [line.strip().split("\t") for line in sys.stdin]
vocab_file.sort(key=lambda x:-int(x[1]))
sys.stdout.write("\n".join("%s\t%s\t%s" % tuple(x) for x in vocab_file))
