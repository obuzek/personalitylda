#!/usr/bin/env python

# Splits the corpus input by document according to the set of splits.
# USAGE: ./inputsplit.py corpus .8 .1 .1
#    creates: corpus.1 corpus.2 corpus.3
# Doc IDs aren't reassigned, so as to be consistent with the corresponding traits file.
#
# Expects a corpus formatted tab-separated, with the docID in the first column.


import sys, random
from collections import defaultdict

# processing arguments
input_corpus = sys.argv[1]
splits = [float(s) for s in sys.argv[2:]]
if sum(splits) - 1.0 > 0.001:
    sys.stdout.write("ERROR: Bad split; must sum to 1.")
    sys.exit(-1)

# identifying documents
user_data = defaultdict(list)

for line in open(input_corpus):
    line_arr = line.split("\t")
    user_data[int(line_arr[0])].append(line)

# calculating how many users belong in each split
users = user_data.keys()
num_users = len(users)

users_per_split = [int(num_users * float(s)) for s in splits]

ctr = 0
while sum(users_per_split) < num_users:
    users_per_split[ctr] += 1
    ctr += 1

# splitting up users in accordance with users_per_split
users_left = users

users_in_split = defaultdict(list)

for i in range(len(splits)):
    for t in range(int(users_per_split[i])):
        user_idx = random.randint(0,len(users_left)-1)
        users_in_split[i].append(users_left.pop(user_idx))

    corpus_out = open(input_corpus + "." + str(i+1), "w+")
    for user in sorted(users_in_split[i]):
        for line in user_data[user]:
            corpus_out.write(line)
        corpus_out.flush()
    corpus_out.close()

