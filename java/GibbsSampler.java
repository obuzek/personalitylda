import java.util.Random; // might be better to use Math.random()?
import java.lang.Math;
import java.util.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public abstract class GibbsSampler {
  // GibbsSampler implements operations for Gibbs sampling in LDA model.

  // parameters.
  double[] alpha;
  double beta;

  // corpus sizes
  int Ntrain; // total number of tokens in train
  int Ntest; // total number of tokens in test
  int Dtrain; // total number of documents in train
  int Dtest; // total number of documents in test
  int K; // total number of topics
  int V; // total number of word types in train set, plus one for OOV.
  int C; // total number of corpora (we're keeping this for now, but don't need it, really).

  // arrays for storing counts
  int[][] CDT; // Dtrain*K entries
  int[][] CWT; // V*K entries
  // test data doesn't keep word counts, only document counts.
  int[][] CDTtest; // Dtest*K entries

  // tables for looking up commonly-needed quantities.
  double[] docTerm_denomLookup;
  double[] docTerm_denomLookup_test;
  double[] wordTerm_denomLookup;

  // array for storing topic assignments and document information
  // TOPIC_ASSIGNMENTS is a 3*Ntrain-long array, which is actually
  // a matrix. Each 4-element row of this matrix is
  // [docID wordID z_di].
  int[][] TOPIC_ASSIGNMENTS;
  // analogously for TOPIC_ASSIGNMENTStest.
  int[][] TOPIC_ASSIGNMENTStest;

  // arrays for looking up corpus for each document.
  int[] TRAIN_CORPORA;
  int[] TEST_CORPORA;
  // INFO matrices let us look up the number of tokens in each document. 
  int[] TRAIN_INFO;
  int[] TEST_INFO;

  // array for storing log likelihoods.
  double[] TRAINLL;
  double[] TESTLL;

  // storing times, next to log likelihoods
  long[] TIMES;

  // arrays for storing running averages of the distributions.
  double[][] THETA; // same dimension as CDT.
  double[][] THETA_test;
  double[][] PHI; // same dimension as CWT

  // Class for generating samples.
  ObuzekRandom obrdm;

  private class CorpusInfo {
      final int corpusID;
      HashMap<Integer,DocInfo> documents;
      int trainOrTest;
      int corpLength;
      boolean corpusChanged;
      public CorpusInfo(int corpusID, int trainOrTest) { this.corpusID = 0;
                                                         this.trainOrTest = trainOrTest;
                                                         documents = new HashMap<Integer,DocInfo>();
                                                         corpLength = 0;
                                                         corpusChanged = false; }
      public void addDoc(DocInfo di) { documents.put(di.docID, di); corpusChanged = true; }
      public DocInfo getDoc(int docID) {
          if (documents.containsKey(docID)) { return documents.get(docID); }
          else { DocInfo di = new DocInfo(docID);
                 documents.put(docID,di);
                 corpusChanged = true;
                 return di; }
      }
      public int corpLength() {
          if(!corpusChanged) { return corpLength; }
          int total = 0;
          for ( DocInfo di : documents.values() ) {
              total += di.docLength;
          }
          corpLength = total;
          corpusChanged = false;
          return total;
      }
      public void fill(int startIndex, int[][] topic_assign, int[] corpora, int[] info) {

            // for every instance of a token in a document, add it to topic_assign
            int passed = 0;
            for ( Integer docID : documents.keySet() ) {
                DocInfo di = documents.get(docID);
                for ( Integer wordID : di.tokenCounts.keySet()) {
                    int wordCount = di.tokenCounts.get(wordID);
                    int stopIndex = startIndex + passed + wordCount;
                    for(int idx = startIndex + passed; idx < stopIndex; idx ++ ) {
                        topic_assign[idx] = new int[] { docID, (int) wordID, 0, 0 };
                    }
                    passed += wordCount;
                }
                // identify every document's corpus
                corpora[docID] = corpusID;

                // identify the doc length
                info[docID] = di.docLength;
            }  

      }

  }

  private class DocInfo {
      final int docID;
      int docLength;
      HashMap<Integer,Integer> tokenCounts;
      public DocInfo(int docID) { this.docID = docID;
                                  tokenCounts = new HashMap<Integer,Integer>(); }
      public void addToken(int wordID, int wordCount) { tokenCounts.put(wordID, wordCount);
                                                        docLength += wordCount; }

  }

  // CONSTRUCTOR!
  public GibbsSampler() {
    return;
  }
  public GibbsSampler(Integer[][] corpus, int K, double alpha, double beta) {
    // read the information from the train file and test file.
    // use this information to initialize all the arrays above.
    // Specifically:
    //	Count number of documents in train
    //	Count number of documents in test
    //	Count number of tokens in train
    //	Count number of tokens in test
    //	Assign universally unique wordIDs (and a way to get back from word IDs
    //		to words later).

    for (int k=0; k<this.alpha.length; k++) {
        this.alpha = alpha;
    }
    this.beta = beta;
    this.K = K;

    HashMap<Integer,CorpusInfo> corpora = new HashMap<Integer,CorpusInfo>();
    for (int lineCtr = 0; lineCtr < corpus.length; lineCtr++) {
        int docID = corpus[lineCtr][0];
        int corpID = corpus[lineCtr][1];
        int trainOrTest = corpus[lineCtr][2];
        int wordID = corpus[lineCtr][3];
        int wordCount = corpus[lineCtr][4];

        int corpIndex = corpID * 10 + trainOrTest;
        CorpusInfo ci;
        if (corpora.containsKey(corpIndex)) { ci = corpora.get(corpIndex); }
        else { ci = new CorpusInfo(corpID, trainOrTest);
            corpora.put(corpIndex, ci); }

        DocInfo doc = ci.getDoc(docID);
        doc.addToken(wordID,wordCount);
        this.V = Math.max(this.V, wordID);
    }

    this.V += 1;
    this.C += 1; 

    int[] TRAIN_CORP_LENGTH = new int[this.C];
    int[] TEST_CORP_LENGTH = new int[this.C];

    for( Integer corpIndex : corpora.keySet() ) {
        CorpusInfo ci = corpora.get(corpIndex);
        int trainOrTest = corpIndex % 10;
        int corpID = corpIndex / 10;
        if (trainOrTest == 0) {
            this.Ntrain += ci.corpLength();// total length of corpus
            TRAIN_CORP_LENGTH[corpID] = ci.corpLength();
            this.Dtrain += ci.documents.size(); // number of documents in corpus
        } else {
            this.Ntest += ci.corpLength();
            TEST_CORP_LENGTH[corpID] = ci.corpLength();
            this.Dtest += ci.documents.size();
        }
    }
    
    this.TOPIC_ASSIGNMENTS = new int[Ntrain][]; // [docID wordID topic x]
    this.TOPIC_ASSIGNMENTStest = new int[Ntest][];
    
    this.TRAIN_CORPORA = new int[Dtrain]; // what corpus is a document in?
    this.TEST_CORPORA = new int[Dtest];

    this.TRAIN_INFO = new int[Dtrain]; // how many words in the document?
    this.TEST_INFO = new int[Dtest];

    for( Integer corpIndex : corpora.keySet() ) {
        CorpusInfo ci = corpora.get(corpIndex);
        int trainOrTest = corpIndex % 10;
        int corpID = corpIndex / 10;

        int startIndex = 0;
        if (trainOrTest == 0) { 
            for(int c = 0; c < corpID; c ++) { startIndex += TRAIN_CORP_LENGTH[c]; }
            ci.fill(startIndex, TOPIC_ASSIGNMENTS,
                    TRAIN_CORPORA,
                    TRAIN_INFO);
        } else {
            for(int c = 0; c < corpID; c ++) { startIndex += TEST_CORP_LENGTH[c]; }
            ci.fill(startIndex, TOPIC_ASSIGNMENTStest,
                    TEST_CORPORA,
                    TEST_INFO);
        }
    }

    // Now, use those counts to initialize arrays.
    this.CDT = new int[Dtrain][K];
    this.CWT = new int[V][K];
    this.CDTtest = new int[Dtest][K];

    // and initialize the wordTerm and docTerm lookups.
    this.docTerm_denomLookup = new double[Dtrain];
    this.docTerm_denomLookup_test = new double[Dtest];
    this.wordTerm_denomLookup = new double[this.K];

    // and the arrays where we'll keep the running averages.
    this.THETA = new double[this.Dtrain][this.K];
    this.THETA_test = new double[this.Dtest][this.K];
    this.PHI = new double[this.V][this.K];

  }

  void run(int T, int Tburn) {
    // run T iterations of sampling, saving information for all iterations
    // after Tburn has passed.

    // initialize counts and initialize the arrays where we'll store
    // likelihoods.
    this.TRAINLL = new double[T];
    this.TESTLL = new double[T];
    this.TIMES = new long[T];

    // initialize counts randomly.
    init_counts();

    // do T sample iterations.
    int nSamples = 0; // track how many samples we've taken.
    // we need a place to store MAP estimates after each iteration.
    double[][] thetaHat;
    double[][] phiHat;
    double[][] thetaHat_test; // test set has its own theta.

    long start_time = System.currentTimeMillis();
    for (int t=0; t<T; t++) {
      // do a sampling iteration on the train data.
      do_sample_iter();

      // compute the MAP estimates based on the current counts.
      thetaHat = mapEstimate_theta(0);
      phiHat = mapEstimate_phi();
      
      // compute train and test log likelihood.
      TRAINLL[t] = compute_LL(thetaHat, phiHat, 0);

      // Resample the test set's topic assignments.
      do_sample_iter_phiFixed(phiHat);
      // and get a MAP estimate of THETAtest.
      thetaHat_test = mapEstimate_theta(1);
      TESTLL[t] = compute_LL(thetaHat_test, phiHat, 1);

      if (t >= Tburn) {
        // keep these estimates as a sample.
        updateRunningAverages(thetaHat, thetaHat_test, phiHat, nSamples);
        nSamples++;
      }

      // record elapsed time
      TIMES[t] = System.currentTimeMillis() - start_time;

    }

    double ll_on_test_from_avg = compute_LL(this.THETA_test, this.PHI, 1);
    String ll_on_test = String.format("%6.13e",ll_on_test_from_avg);
    System.out.println("Log-likelihood:\t"+ll_on_test);
  }

  // method to initialize the count arrays based on TRAINMX and TESTMX
  // (see gibbs_init_counts.m)
  void init_counts() {
    /* this method fills in the z_di entries in TOPIC_ASSIGNMENTS
     and TOPIC_ASSIGNMENTStest. This assumes that we've already
     initialized those matrices, and that we've already filled
     in their document ID and word ID information. */

    Random rdm = new Random();

    // fill in TOPIC_ASSIGNMENTS and associated count tables.
    // each "row" of TOPIC_ASSIGNMENTS is [docID wordID z_di]
    int docID; int wordID; int corpID;
    for (int i=0; i<this.Ntrain; i++) {
      docID = this.TOPIC_ASSIGNMENTS[i][0];
      wordID = this.TOPIC_ASSIGNMENTS[i][1];
      // sample z_di as a multinomial with uniform distribution.
      this.TOPIC_ASSIGNMENTS[i][2] = rdm.nextInt(this.K);

      // update main count tables.
      int k = this.TOPIC_ASSIGNMENTS[i][2];

      this.CDT[docID][k] += 1;
      this.CWT[wordID][k] += 1;
    }

    // fill in TOPIC_ASSIGNMENTStest and associated count tables.
    for (int i=0; i<Ntest; i++) {
      docID = TOPIC_ASSIGNMENTStest[i][0];
      wordID = TOPIC_ASSIGNMENTStest[i][1];
      // sample z_di as unif. multinom.
      TOPIC_ASSIGNMENTStest[i][2] = rdm.nextInt(K);

      // update main count tables accordingly.
      int k = TOPIC_ASSIGNMENTStest[i][2];
      this.CDTtest[docID][k] += 1;
    }

    // there are a few quantities we're going to need on hand a whole
    // bunch of times. Calculate them.

    // for every docID, I'd like to be able to sum over the topics,
    // counting how many tokens were assigned to each topic. This is
    // just the number of tokens in that docID.
    // This information is already stored in TRAIN_INFO
    // and TEST_INFO.
    for (int d=0; d<this.Dtrain; d++) {
      this.docTerm_denomLookup[d] = this.TRAIN_INFO[d] + this.K*this.alpha;
    }
    for (int d=0; d<this.Dtest; d++) {
      this.docTerm_denomLookup_test[d] = this.TEST_INFO[d] + this.K*this.alpha;
    }

    // for every topic, I'd like to know how many words in total were
    // assigned to that topic plus smoothing term.
    for (int k=0; k<this.K; k++) {
      this.wordTerm_denomLookup[k] = 0;
      for (int w=0; w<this.V; w++) {
        this.wordTerm_denomLookup[k] += this.CWT[w][k];
      }
      this.wordTerm_denomLookup[k] += this.V*this.beta;
    }
  }

  // compute the likelihood of the train data.
  double compute_LL(double[][] thetaHat, double[][] phiHat, int trainOrTest) {
    // thetaHat is D by K.
    // phiHat is V by K

    double ll_total = 0.0; // we'll accumulate here.

    int docID; int wordID;
    int Nupperlim;
    int[][] TOPASSIGNS;

    if ( trainOrTest==0 ) {
      Nupperlim = Ntrain;
      TOPASSIGNS = this.TOPIC_ASSIGNMENTS;
    } else { // 1, do test.
      Nupperlim = Ntest;
      TOPASSIGNS = this.TOPIC_ASSIGNMENTStest;
    }

    double lik_thisToken;
    for (int i=0; i<Nupperlim; i++) {
      docID = TOPASSIGNS[i][0];
      wordID = TOPASSIGNS[i][1];

      // calculate the probability of this word under the model.
      // First, sum over all topics.
      lik_thisToken = 0.0;
      for (int z=0; z<K; z++) {
        double phi = phiHat[wordID][z];
        double th = thetaHat[docID][z];
        lik_thisToken += th*phi;
      }
      ll_total += Math.log(lik_thisToken);
    }
    return ll_total;
  }

  double[][] mapEstimate_theta(int trainOrTest) {
    int[][] CDTcounts;
    int D;
    double[] dt_denomLookup;
    double numer; double denom;
    if (trainOrTest==0) { // 0 is train data.
      CDTcounts = this.CDT;
      D = this.Dtrain;
      dt_denomLookup = this.docTerm_denomLookup;
    } else { //otherwise we're doing test data.
      CDTcounts = this.CDTtest;
      D = this.Dtest;
      dt_denomLookup = this.docTerm_denomLookup_test;
    }

    double[][] theta = new double[D][this.K];
    for (int d=0; d<D; d++) {
      for (int k=0; k<this.K; k++) {
        numer = CDTcounts[d][k] + this.alpha;
        denom = dt_denomLookup[d];
        theta[d][k] = numer/denom;
      }
    }
    return theta;
  }

  double[][] mapEstimate_phi() {
    double[][] phi = new double[this.V][this.K];

    double numer; double denom;
    for (int w=0; w<this.V; w++) {
      for (int k=0; k<this.K; k++) {
        numer = this.CWT[w][k] + this.beta;
        denom = this.wordTerm_denomLookup[k];
        phi[w][k] = numer/denom;
      }
    }

    return phi;
  }

  void updateRunningAverages(double[][] thetaHat, double[][] thetaHat_test,
				double[][] phi, int nSamples) {
    // Given three new point estimates, and the number of samples we've
    // already taken, update the running averages stored in
    // this.THETA, this.THETA_test, this.PHI_GLOBAL and this.PHI_CORPUS.
    double n = (double) nSamples;
    double oldWeight = n/(n+1);
    double newWeight = 1/(n+1);
    for (int k=0; k<this.K; k++) {
      for (int d=0; d<this.Dtrain; d++) {
        this.THETA[d][k] = oldWeight*this.THETA[d][k]+newWeight*thetaHat[d][k];
      }
      for (int d=0; d<this.Dtest; d++) {
        this.THETA_test[d][k] = oldWeight*this.THETA_test[d][k]
				+ newWeight*thetaHat_test[d][k];
      }
      for (int w=0; w<this.V; w++) {
        this.PHI[w][k] = oldWeight*this.PHI[w][k] + newWeight*phi[w][k];
      }
    }
  }

  // print the information that Suchi et al. want
  void print_info(String output_basename) {
    /* There are 6 different files that we need to create and print to.
	output_basename-theta will contain the values of the THETA matrix
		(combining both the train and test documents?), where the
		d-th line corresponds to the d-th document, in the same
		order that the documents appear in the input file. The
		k-th token of the d-th line is THETA_{d,k}.
		A pitfall here is that we don't know the original order
		of the documents in the input files. So we'll print them
		in order from our tables, first the Dtrain documents
		from the training set, then the Dtest documents from the
		test set, both in order of docIDs. We'll leave it to the
		post-processing script to sort things out.
	output_basename-phi will contain the values of PHI_GLOBAL. Each line
		corresponds to a word, where the first token of the line is
		the word (we'll print the wordID and let our post-proc
		scripts figure the rest out), and the remaining tokens contain
		the value of PHI_GLOBAL[w][k]
	output_basename-phi0 is similar, but prints PHI_CORPUS[w][0][k]
	output_basename-phi1 is similar, but prints PHI_CORPUS[w][1][k]
	output_basename-trainll contains log-likelihoods of the train data,
		one number per line, one line for each sample iteration.
	output_basename-testll is similar but has test log-likelihoods.
    */
    // print the theta stuff.
    print_thetaInfo(output_basename);
    print_phiInfo(output_basename);
    print_likelihoods(output_basename);
  }

  void print_thetaInfo(String output_basename) {
    // print the THETA and THETAtest information.
   try {
    File f = new File(output_basename+"-theta");
    if (!f.exists()) {
      f.createNewFile();
    }

    FileWriter fw = new FileWriter(f.getAbsoluteFile());
    BufferedWriter bw = new BufferedWriter(fw);
    double numberToPrint; String printMe;
    for (int d=0; d<this.Dtrain; d++) {
      numberToPrint = this.THETA[d][0];
      printMe = String.format("%6.13e",numberToPrint);
      bw.write(printMe);
      for (int k=1; k<this.K; k++) {
        numberToPrint = this.THETA[d][k];
        printMe = String.format("%6.13e", numberToPrint);
        bw.write(" "+printMe);
      }
      bw.write("\n");
    }
    for (int d=0; d<this.Dtest; d++) {
      numberToPrint = this.THETA_test[d][0];
      printMe = String.format("%6.13e",numberToPrint);
      bw.write(printMe);
      for (int k=1; k<this.K; k++) {
        numberToPrint = this.THETA_test[d][k];
        printMe = String.format("%6.13e", numberToPrint);
        bw.write(" "+printMe);
      }
      bw.write("\n");
    }
    bw.close();
  } catch (IOException e) {
    System.err.println(e.getMessage());
  }//end trycatch
  }//endfunction

  void print_phiInfo(String output_basename) {
    // print the PHI information.
    double numberToPrint; String printMe;

    try { 
      // first, the PHI_GLOBAL
      File f = new File(output_basename+"-phi");
      if (!f.exists()) {
        f.createNewFile();
      }
      FileWriter fw = new FileWriter(f.getAbsoluteFile());
      BufferedWriter bw = new BufferedWriter(fw);
      for (int w=0; w<this.V; w++) {
        printMe = String.format("%d", w);
        bw.write(printMe);
        for (int k=0; k<this.K; k++) {
          numberToPrint = this.PHI[w][k];
          printMe = String.format("%6.13e",numberToPrint);
          bw.write(" "+printMe);
        }
        bw.write("\n"); 
      }
      bw.close();
    } catch (IOException e) {
      System.err.println(e.getMessage());
    }
  }

  void print_likelihoods(String output_basename) {
    // print train and test likelihoods to separate files.
    double numberToPrint; String printMe;

   try{
    File f = new File(output_basename+"-trainll");
    if (!f.exists()) {
      f.createNewFile();
    }
    FileWriter fw = new FileWriter(f.getAbsoluteFile());
    BufferedWriter bw = new BufferedWriter(fw);
    for (int i=0; i<this.TRAINLL.length; i++) {
      numberToPrint = this.TRAINLL[i];
      printMe = String.format("%6.13e",numberToPrint);
      bw.write(printMe+"\n");
    }
    bw.close();

    f = new File(output_basename+"-testll");
    if (!f.exists()) {
      f.createNewFile();
    }
    fw = new FileWriter(f.getAbsoluteFile());
    bw = new BufferedWriter(fw);
    for (int i=0; i<this.TESTLL.length; i++) {
      numberToPrint = this.TESTLL[i];
      printMe = String.format("%6.13e",numberToPrint);
      bw.write(printMe+"\n");
    }
    bw.close();
    
    f = new File(output_basename+"-times");
    if (!f.exists()) {
      f.createNewFile();
    }
    fw = new FileWriter(f.getAbsoluteFile());
    bw = new BufferedWriter(fw);
    for (int i=0; i<this.TIMES.length; i++) {
      numberToPrint = this.TIMES[i];
      printMe = String.format("%6.13e",numberToPrint);
      bw.write(printMe+"\n");
    }
    bw.close();
    } catch (IOException e) {
    System.err.println(e.getMessage());
  }//end trycatch
  }//endfunctiondef

  // perform one iteration of sampling on the train data
  abstract void do_sample_iter();

  // perform one iteration of sampling on the test data.
  // only requires parameters alpha and bet
  abstract void do_sample_iter_phiFixed(double[][] phi);

  int sampleBinom(double lambda) {
    return this.obrdm.sampleBinom(lambda);
  }

  // generate from a multinomial
  int multinom(double[] prob) {
    return this.obrdm.multinom(prob);
  }

  void printMatrix(double[][] matrix, int m, int n) {
    for (int i=0; i<m; i++) {
      for (int j=0; j<n; j++) {
        System.out.print(matrix[i][j]+" ");
      }
      System.out.println();
    }
  }

  double[][] getThetaTrain() {
    return this.THETA;
  }
  double[][] getThetaTest() {
    return this.THETA_test;
  }
}
