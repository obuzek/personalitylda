import java.util.Random; // might be better to use Math.random()?
import java.lang.Math;
import java.util.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import org.apache.commons.math3.special.Gamma;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.BetaDistribution;

public abstract class DMRSampler extends PersonalityModel{
  // DMRSampler implements operations for Gibbs sampling in DMR model.

  // hyperhyperparameters
  double lambda_variance;
  double lambda_mean;

  // hyperparameters
  double beta;
  double[][] alphaDK;
  double[] beta_alpha; // one per trait
  double[] beta_gamma; // one per trait
  double[] p_trait;
  boolean binary_data;

  //parameters
  int K; // total number of topics
  int F; // F = num features / traits
  double[][] lambdaKF; // K*F entries (where F = num features / traits)

  // corpus sizes
  transient int N; // total number of tokens in train
  transient int D; // total number of documents in train
  // int V; // total number of word types in train set, plus one for OOV. (now inherited from PersonalityModel)

  // arrays for storing counts
  transient int[][] CDT; // D*K entries - also expresses n_{t | d}
  int[][] CWT; // V*K entries

  // tables for looking up commonly-needed quantities.
  transient double[] docTerm_denomLookup;
  // double[] docTerm_denomLookup_dev;
  double[] wordTerm_denomLookup;
  transient double[] sumAlphaOverTopicK;

  // array for storing topic assignments and document information
  // TOPIC_ASSIGNMENTS is a 3*Ntrain-long array, which is actually
  // a matrix. Each 4-element row of this matrix is
  // [docID wordID z_di].
  transient int[][] TOPIC_ASSIGNMENTS;

  transient double[][] TRAITS; // D * num_features
  transient double[][] SUPER_TRAITS;

  // INFO matrices let us look up the number of tokens in each document. 
  transient int[] DOC_LENGTH;

  // array for storing log likelihoods.
  transient double[] LOG_LIKELIHOOD;

  // storing times, next to log likelihoods
  transient long[] TIMES;

  // arrays for storing running averages of the distributions.
  transient double[][] THETA; // same dimension as CDT.
  double[][] PHI; // same dimension as CWT
  double[][] LAMBDA;

  // iters
  // transient int T;
  // transient int Tburn;

  Random rdm;

  transient Mode mode;

  int SCALE_FACTOR;

  // CONSTRUCTOR!
  public DMRSampler(int K, double lambda_variance, double beta, boolean binary_data) {
    // read the information from the train file and dev file.
    // use this information to initialize all the arrays above.
    // Specifically:
    //	Count number of documents in train
    //	Count number of documents in dev 
    //	Count number of tokens in train
    //	Count number of tokens in dev
    //	Assign universally unique wordIDs (and a way to get back from word IDs
    //		to words later).

    this.lambda_variance = lambda_variance;
    this.lambda_mean = 0.0;
    this.beta = beta;
    this.K = K;
    this.V = -1;
    this.binary_data = binary_data;

    this.SCALE_FACTOR = 1;

    System.out.println("Initializing Model: K="+K+", Beta="+beta+", lambda_var="+lambda_variance);
  }
  private void initialize_from_corpus(int[][] corpus, double[][] traits, double[][] features) {

    this.N = 0;
    if ( mode == Mode.TRAIN)
        this.F = traits[0].length;
    beta_alpha = new double[F];
    beta_gamma = new double[F];
    //System.out.println(traits.length);
    //System.out.println(traits[0].length);
   
    int docID = 0;
    for (int lineCtr = 0; lineCtr < corpus.length; lineCtr++) {
        docID = corpus[lineCtr][0];
        int wordID = corpus[lineCtr][1];
        int wordCount = corpus[lineCtr][2];

        if ( mode == Mode.TRAIN)
            this.V = Math.max(this.V, wordID);
        else
            corpus[lineCtr][1] = this.V;

        this.D = Math.max(this.D, docID);
        this.N += wordCount;
    }

    if ( mode == Mode.TRAIN)
        this.V += 1;

    this.D = this.D + 1;
   
    TOPIC_ASSIGNMENTS = new int[N][]; // [docID wordID topic x]
    
    TRAITS = new double[this.D][F];
    System.out.println(this.D + " " + this.F);
    if ( mode == Mode.TRAIN) {
        for(int d = 0; d < this.D; d++) {
            for(int f = 0; f < F; f++) {
                TRAITS[d][f] = traits[d][f] * SCALE_FACTOR;
            }
        }
    }
    
    DOC_LENGTH = new int[D]; // how many words in the document?

    alphaDK = new double[D][K];
    sumAlphaOverTopicK = new double[D];

    int ctr = 0;
    for(int lineCtr = 0; lineCtr < corpus.length; lineCtr++) {
        docID = corpus[lineCtr][0];
        int wordID = corpus[lineCtr][1];
        int wordCount = corpus[lineCtr][2]; 
        for(int word = 0; word < wordCount; word++) {
            TOPIC_ASSIGNMENTS[ctr] = new int[] { docID, wordID, 0, 0 };
            ctr++;
        }
        DOC_LENGTH[docID] += wordCount;
    } 

    // Now, use those counts to initialize arrays.
    this.CDT = new int[D][K];

    // and initialize the wordTerm and docTerm lookups.
    this.docTerm_denomLookup = new double[D];

    // and the arrays where we'll keep the running averages.
    this.THETA = new double[this.D][this.K];

    // starting the random number generator
    this.rdm = new Random();
    if ( mode == Mode.TRAIN) {
       // Now, use those counts to initialize arrays.
      this.p_trait = new double[F];
      this.CWT = new int[V+1][K];

      this.lambdaKF = new double[K][F];
      this.wordTerm_denomLookup = new double[this.K];

      // and the arrays where we'll keep the running averages.
      this.PHI = new double[this.V+1][this.K];
      this.LAMBDA = new double[this.K][this.F];
    }
  }

  void gradient_ascent_beta_alpha(double[] traitSumLnX, double lrnRate) {
      for (int i = 0; i < beta_alpha.length; i++) {

          double grad_alpha = traitSumLnX[i] + D * (- Gamma.digamma(beta_alpha[i] + beta_gamma[i])
                                                                    + Gamma.digamma(beta_alpha[i]));

          beta_alpha[i] += lrnRate * grad_alpha;

      }
  }

  void gradient_ascent_beta_gamma(double[] traitSumLn1MinX, double lrnRate) {
     for (int i = 0; i < beta_gamma.length; i++) {

          double grad_gamma = traitSumLn1MinX[i] + D * (- Gamma.digamma(beta_alpha[i] + beta_gamma[i])
                                                                    + Gamma.digamma(beta_gamma[i]));

          beta_gamma[i] += lrnRate * grad_gamma;

      }

  }

  private void estimate_trait_prior(double[][] traits) {
      if (binary_data)
          estimate_binomial_params(traits);
      else
          estimate_beta_params(traits);
  }

  private void estimate_binomial_params(double[][] traits) {
      System.out.println("ESTIMATING!!!!!!!!!!");
      for (int trait = 0; trait < F; trait ++ ) {
          for (int doc = 0; doc < D; doc ++ ) {
              p_trait[trait] += traits[doc][trait] / SCALE_FACTOR;
          }
          p_trait[trait] /= traits.length;
          System.out.println(p_trait[trait]);
      }

  }


  private void estimate_beta_params(double[][] traits) {

    double lrnRate = 0.001;
    double iters = 250;

    double[] traitSumLnX = new double[traits[0].length];
    double[] traitSumLn1MinX = new double[traits[0].length];

    for (int trait = 0; trait < F; trait ++ ) {
        beta_alpha[trait] = rdm.nextDouble();
        beta_gamma[trait] = rdm.nextDouble();
    }

    for (int doc = 0; doc < traits.length; doc ++ ) {
        for(int trait = 0; trait < traits[0].length; trait ++) {
            traitSumLnX[trait] += Math.log(traits[doc][trait]);
            traitSumLn1MinX[trait] += Math.log(1 - traits[doc][trait]);
        }
    }

    for(int iter = 0; iter < iters; iter ++ ) {
        
        gradient_ascent_beta_alpha(traitSumLnX, lrnRate);

        gradient_ascent_beta_gamma(traitSumLn1MinX, lrnRate);

    }

  }

  public void train(int[][] trainCorpus, double[][] traits, double[][] features, int T, int Tburn) {

    mode = Mode.TRAIN;

    initialize_from_corpus(trainCorpus, traits, features);

    // run T iterations of sampling, saving information for all iterations
    // after Tburn has passed.

    // initialize counts and initialize the arrays where we'll store
    // likelihoods.
    this.LOG_LIKELIHOOD = new double[T];
    this.TIMES = new long[T];

    // initialize counts randomly.
    init_counts();

    // do T sample iterations.
    int nSamples = 0; // track how many samples we've taken.
    // we need a place to store MAP estimates after each iteration.
    double[][] thetaHat;
    double[][] phiHat;

    // estimate Beta params from trait values
    estimate_trait_prior(traits);

    //System.out.println("LAMBDA:");
    //printMatrix(lambdaKF,K,F);
    //System.out.println("SOME ALPHA:");
    //printMatrix(alphaDK,20,K);
    //System.out.println("SOME TRAITS:");
    //printMatrix(TRAITS,20,F);
    long start_time = System.currentTimeMillis();
    for (int t=0; t<T; t++) {
      // resample lambdas based on current alpha and theta
      // implicitly updates alphaDK
      do_sample_lambda_iter(0.0001); // arg is step size

      // do a sampling iteration on the train data.
      do_sample_topics_iter();

      // compute the MAP estimates based on the current counts.
      thetaHat = mapEstimate_theta(); // arg reprs train or dev
      phiHat = mapEstimate_phi();
      
      // compute train and dev log likelihood.
      LOG_LIKELIHOOD[t] = compute_LL(thetaHat, phiHat);

      if (t >= Tburn) {
        // keep these estimates as a sample.
        updateRunningAverages(thetaHat, phiHat, lambdaKF, nSamples);
        nSamples++;
      }

      // record elapsed time
      TIMES[t] = System.currentTimeMillis() - start_time;

    }

    //System.out.println("LAMBDA:");
    //printMatrix(lambdaKF,K,F);
    //System.out.println("SOME ALPHA:");
    //printMatrix(alphaDK,20,K);

    //double ll_on_dev_from_avg = compute_LL(this.THETA, this.PHI, Mode.DEV);
    //String ll_on_dev = String.format("%6.13e",ll_on_dev_from_avg);
    //System.out.println("Log-likelihood:\t"+ll_on_dev);
  }

  public double[][] test(int[][] testCorpus, double[][] features, int T, int Tburn) {
    
    mode = Mode.TEST;

    initialize_from_corpus(testCorpus, null, features);
    init_counts();

    double[][] thetaHat;
    double[][] phiHat;

    //System.out.println("TEST LAMBDA");
    //printMatrix(lambdaKF,K,F);
    //System.out.println("SOME ALPHA:");
    //printMatrix(alphaDK,20,K);
    //System.out.println("TEST X???");
    //printMatrix(TRAITS,20,F);
    //System.out.println("P TRAITS");
    for(int i=0;i<F;i++)
        System.out.println(p_trait[i]);

    SUPER_TRAITS = new double[TRAITS.length][TRAITS[0].length];

    int nSamples = 0;
    for (int t=0; t<T; t++) {
        System.out.println("ITER #"+t);

        // resample theta 
        thetaHat = mapEstimate_theta();
        //System.out.println("THETA HAT");
        //printMatrix(thetaHat,10,K);
        do_sample_topics_iter_phiFixed();
        //System.out.println("THETA");
        //printMatrix(THETA,10,K);

        // interpolate between theta and lambda * X
        //  ALGORITHM:
        //    1. Determine what alpha should look like according to (normalized) lambda * X.
        //    2. Determine what alpha should look like according to (normalized) theta.
        //    3. Interpolate a value for alpha between what lambda * X think and what theta thinks.
        //    4. Re-normalize alpha according to the normalization constant from lambda * X.
        do_alpha_interpolation(thetaHat);
        //System.out.println("ALPHA");
        //printMatrix(alphaDK,10,K);

        // grid search to recompute x
        //   use the prior probability on x and the value of alpha - x * lambda to determine the best
        //   value of the traits going forward
        if (binary_data)
            do_traits_grid_search_binary();
        else
            do_traits_grid_search_beta();
          if (t >= Tburn) {
            // keep these estimates as a sample.
            updateRunningAverages(thetaHat, null, lambdaKF, nSamples);
            nSamples++;
          }


        System.out.println("TRAITS");
        printMatrix(TRAITS,10,F);

        System.out.println();

    }

    double[][] traits = new double[TRAITS.length - 1][TRAITS[0].length];
    for (int i =0; i < TRAITS.length - 1; i ++) {
        for (int j = 0; j < TRAITS[0].length; j++) {
            traits[i][j] = TRAITS[i][j];
        }
    }
    return traits; 
  }

  void do_alpha_interpolation(double[][] thetaHat) {
      double[] alpha_1_norm = new double[D];
      double[] alpha_2_norm = new double[D];

      double[][] alpha_1 = new double[D][K];
      for ( int doc = 0; doc < D; doc ++ ) {
          for ( int topic = 0; topic < K; topic ++ ) {
              alpha_1[doc][topic] = 0;
              for ( int trait = 0; trait < F; trait ++ ) {
                  alpha_1[doc][topic] += lambdaKF[topic][trait] * TRAITS[doc][trait];
              }
              alpha_1[doc][topic] = Math.exp(alpha_1[doc][topic]);
              alpha_1_norm[doc] += alpha_1[doc][topic];
              alpha_2_norm[doc] += thetaHat[doc][topic];
          }
      }

      for ( int doc = 0; doc < D; doc ++ ) {
          for ( int topic = 0; topic < K; topic ++ ) {
              alphaDK[doc][topic] = 0.5*(alpha_1[doc][topic]) + 0.5*(thetaHat[doc][topic] / alpha_2_norm[doc] * alpha_1_norm[doc]);
          }
      }
      //System.out.println("alphaDK");
      //printMatrix(alphaDK,10,alphaDK[0].length);
      //System.out.println("alpha_1");
      //printMatrix(alpha_1,10,K);
      //System.out.println();
  }

  void do_traits_grid_search_binary() {
      double[][] trait_prob = new double[F][2];
      for (int trait = 0; trait < F; trait ++) {
          for (int i=0; i <= 1; i++) {
              trait_prob[trait][i] = (i==1) ? p_trait[trait] : 1 - p_trait[trait];
          }
      }
//      System.out.println("TRAIT PROB");
      printMatrix(trait_prob,F,2);
      for (int doc = 0; doc < D; doc ++ ) {
      double[] curr_max = new double[]{ -5,-5,-5,-5,-5};
      double curr_max_prob = Double.NEGATIVE_INFINITY;

      for (int t_0 = 0; t_0 <= 1; t_0 += 1) {
          for (int t_1 = 0; t_1 <= 1; t_1 += 1) {
              for (int t_2 = 0; t_2 <= 1; t_2 += 1) {
                  for (int t_3 = 0; t_3 <= 1; t_3 += 1) {
                      for (int t_4 = 0; t_4 <= 1; t_4 += 1) {
                          double l1norm_alpha_diff = 0;
                          double l2norm_alpha_diff = 0;
                          double alpha_diff_prob = 0;
                          for (int topic = 0; topic < K; topic ++ ) {
                              //NormalDistribution nd = new NormalDistribution(alphaDK[doc][topic], 1.0);
                              double predicted_alpha = Math.exp(lambdaKF[topic][0]*t_0 +
                                                lambdaKF[topic][1]*t_1 +
                                                lambdaKF[topic][2]*t_2 +
                                                lambdaKF[topic][3]*t_3 +
                                                lambdaKF[topic][4]*t_4);

                              l1norm_alpha_diff += Math.abs(alphaDK[doc][topic] - predicted_alpha);
                              l2norm_alpha_diff += Math.pow(alphaDK[doc][topic] - predicted_alpha,2);
                              //System.out.println(alphaDK[doc][topic]);
                              //System.out.println(predicted_alpha);
                              //System.out.println("prob of above happening: "+nd.density(predicted_alpha) + "\n");
                              //alpha_diff_prob += Math.log(nd.density(predicted_alpha));
                          }
                          //System.out.println(l1norm_alpha_diff);
                          double prob = Math.log(trait_prob[0][t_0]) + 
                                        Math.log(trait_prob[1][t_1]) +
                                        Math.log(trait_prob[2][t_2]) +
                                        Math.log(trait_prob[3][t_3]) +
                                        Math.log(trait_prob[4][t_4]);
                          //System.out.println(prob);
                          //System.out.println(alpha_diff_prob);
                          prob /=  l2norm_alpha_diff;
                          if (prob > curr_max_prob) {
                              curr_max_prob = prob;
                              curr_max[0] = t_0;
                              curr_max[1] = t_1;
                              curr_max[2] = t_2;
                              curr_max[3] = t_3;
                              curr_max[4] = t_4;
                          }
                    }
                }
            }
        }
      }
    
    for (int i=0; i < F; i++) {
        TRAITS[doc][i] = curr_max[i];
    }
      }

  }
 

  void do_traits_grid_search_beta() {
      double[][] trait_prob = new double[F][(int)((1-0) / 0.1 + 1.0)];
      BetaDistribution[] bd = new BetaDistribution[F];
      for (int trait = 0; trait < F; trait ++ ) {
          bd[trait] = new BetaDistribution(beta_alpha[trait],beta_gamma[trait]);
          for (int x = 0; x <= 10; x ++ ) {
              trait_prob[trait][x] = Math.log(bd[trait].probability( x / 10 ));
          }
      }
      for (int doc = 0; doc < D; doc ++ ) {
      double[] curr_max = new double[F];
      double curr_max_prob = 0.0;

      System.out.println("TRAIT PROB");
      printMatrix(trait_prob,trait_prob.length,trait_prob[0].length);
      for (double t_0 = 0; t_0 <= 1; t_0 += 0.1) {
          for (double t_1 = 0; t_1 <= 1; t_1 += 0.1) {
              for (double t_2 = 0; t_2 <= 1; t_2 += 0.1) {
                  for (double t_3 = 0; t_3 <= 1; t_3 += 0.1) {
                      for (double t_4 = 0; t_4 <= 1; t_4 += 0.1) {
                          double l1norm_alpha_diff = 0;
                          for (int topic = 0; topic < K; topic ++ ) {
                              l1norm_alpha_diff += Math.abs(alphaDK[doc][topic] -
                                                (Math.exp(lambdaKF[topic][0]*t_0) +
                                                lambdaKF[topic][1]*t_1 +
                                                lambdaKF[topic][2]*t_2 +
                                                lambdaKF[topic][3]*t_3 +
                                                lambdaKF[topic][4]*t_4));
                          }
                          double prob = trait_prob[0][(int)(t_0*10)] +
                                        trait_prob[1][(int)(t_1*10)] +
                                        trait_prob[2][(int)(t_2*10)] +
                                        trait_prob[3][(int)(t_3*10)] +
                                        trait_prob[4][(int)(t_4*10)];
                          prob *= Math.log(l1norm_alpha_diff);
                          if (prob > curr_max_prob) {
                              curr_max_prob = prob;
                              curr_max[0] = t_0;
                              curr_max[1] = t_1;
                              curr_max[2] = t_2;
                              curr_max[3] = t_3;
                              curr_max[4] = t_4;
                          }
                    }
                }
            }
        }
    }
    for (int i = 0; i < F; i++) {
        TRAITS[doc][i] = curr_max[i] * SCALE_FACTOR;
    }
    }


  }

  void init_topic_assignments() {
    /* this method fills in the z_di entries in TOPIC_ASSIGNMENTS
     and TOPIC_ASSIGNMENTSdev. This assumes that we've already
     initialized those matrices, and that we've already filled
     in their document ID and word ID information. */

    // fill in TOPIC_ASSIGNMENTS and associated count tables.
    // each "row" of TOPIC_ASSIGNMENTS is [docID wordID z_di]
    int docID; int wordID; int corpID;
    for (int i=0; i<this.N; i++) {
      docID = this.TOPIC_ASSIGNMENTS[i][0];
      wordID = this.TOPIC_ASSIGNMENTS[i][1];
      // sample z_di as a multinomial with uniform distribution.
      this.TOPIC_ASSIGNMENTS[i][2] = this.rdm.nextInt(this.K);

      // update main count tables.
      int k = this.TOPIC_ASSIGNMENTS[i][2];

      this.CDT[docID][k] += 1;
      if ( mode == Mode.TRAIN)
          this.CWT[wordID][k] += 1;
    }

    // there are a few quantities we're going to need on hand a whole
    // bunch of times. Calculate them.

    // for every docID, I'd like to be able to sum over the topics,
    // counting how many tokens were assigned to each topic. This is
    // just the number of tokens in that docID.
    // This information is already stored in DOC_LENGTH
    // and DEV_LENGTH.
    for (int d=0; d<this.D; d++) {
      this.docTerm_denomLookup[d] = this.DOC_LENGTH[d];
      for (int k=0;k<K;k++) {
          this.docTerm_denomLookup[d] += this.alphaDK[d][k];
      }
    }

    if ( mode == Mode.TRAIN) {
        // for every topic, I'd like to know how many words in total were
        // assigned to that topic (according to phi) plus smoothing term.
        for (int k=0; k<this.K; k++) {
          this.wordTerm_denomLookup[k] = 0;
          for (int w=0; w<this.V; w++) {
            this.wordTerm_denomLookup[k] += this.CWT[w][k];
          }
          this.wordTerm_denomLookup[k] += this.V*this.beta;
        }
    }

  }
  private void init_lambdas() {
      NormalDistribution nd = new NormalDistribution(lambda_mean,
                                                     Math.abs(Math.sqrt(lambda_variance)));

      for (int k=0;k<K;k++) {
          for(int f=0;f<F;f++) {
              lambdaKF[k][f] = nd.sample();
          }
      }
      recompute_alpha();
  }
  // method to initialize the count arrays based on TRAINMX and DEVMX
  // (see gibbs_init_counts.m)
  void init_counts() {
      init_topic_assignments();
      if ( mode == Mode.TRAIN)
          init_lambdas();
      if ( mode == Mode.TEST)
          init_x();
  }

  void init_x() {
      rdm = new Random(298475295);
      if (binary_data) {
          int ctr = 0;
          for (int doc = 0; doc < D; doc ++) {
              for (int trait = 0; trait < TRAITS[0].length; trait++) {
                  double rand = rdm.nextDouble();
                  System.out.println(rand);
                  if ( rand > p_trait[trait]) {
                      TRAITS[doc][trait] = SCALE_FACTOR;
                  } else {
                      TRAITS[doc][trait] = 0;
                  }
              }
          }

      } else {
      BetaDistribution[] bd = new BetaDistribution[beta_alpha.length];
      // since we have no idea what the traits should be, let's make a draw from the Beta distribution
      for (int trait = 0; trait < F; trait ++ ) { 
          bd[trait] = new BetaDistribution(beta_alpha[trait], beta_gamma[trait]);
          for (int doc = 0; doc < D; doc ++ ) {
              TRAITS[doc][trait] = bd[trait].sample();
          }
      }
      }

      recompute_alpha();
      printMatrix(alphaDK,10,K);
  }

  private double dot(double[] a, double[] b) {
      double total = 0.0;
      for (int i=0; i < a.length; i++) {
          total += a[i] * b[i];
      }
      return total;
  }

  private void recompute_alpha() {
      for (int d=0;d<D;d++) {
          sumAlphaOverTopicK[d] = 0;
          for (int k=0;k<K;k++) {
              alphaDK[d][k] = Math.exp(dot(TRAITS[d],lambdaKF[k]));
              sumAlphaOverTopicK[d] += alphaDK[d][k];
          }
      }
  }

  // compute the likelihood of the train data.
  double compute_LL_DMR(double[][] lambda) {
    // lambda is K by F

    double ll_total = 0.0; // we'll accumulate here.
  
    for (int docID=0; docID<D; docID++) {
        ll_total += Gamma.logGamma(sumAlphaOverTopicK[docID]);
        ll_total -= Gamma.logGamma(sumAlphaOverTopicK[docID] + DOC_LENGTH[docID]);
        
        for (int k=0;k<K;k++) {
            ll_total += Gamma.logGamma(alphaDK[docID][k] + CDT[docID][k]);
            ll_total -= Gamma.logGamma(alphaDK[docID][k]);
        }
     }

     double negLogSqrt = -Math.log(Math.sqrt(2 * Math.PI * lambda_variance));
     for (int k=0; k<K; k++) {
        for (int f=0;f<F; f++) {
            ll_total += negLogSqrt;
            ll_total += lambdaKF[k][f] * lambdaKF[k][f] / ( 2 * lambda_variance);
        }
     } 
    return ll_total;
  }

  // compute the likelihood of the train data.
  double compute_LL(double[][] thetaHat, double[][] phiHat) {
    // thetaHat is D by K.
    // phiHat is V by K

    double ll_total = 0.0; // we'll accumulate here.
  
    int docID; int wordID;
    double lik_thisToken;
    for (int i=0; i<N; i++) {
      docID = TOPIC_ASSIGNMENTS[i][0];
      wordID = TOPIC_ASSIGNMENTS[i][1];

      // calculate the probability of this word under the model.
      // First, sum over all topics.
      lik_thisToken = 0.0;
      for (int z=0; z<K; z++) {
        double phi = phiHat[wordID][z];
        double th = thetaHat[docID][z];
        lik_thisToken += th*phi;
      }
      ll_total += Math.log(lik_thisToken);
    }
    return ll_total;
  }

  double[][] mapEstimate_theta() {
    double numer; double denom;

    double[][] theta = new double[D][this.K];
    for (int d=0; d<D; d++) {
      for (int k=0; k<this.K; k++) {
        numer = CDT[d][k] + this.alphaDK[d][k];
        denom = docTerm_denomLookup[d];
        theta[d][k] = numer/denom;
      }
    }
    return theta;
  }

  double[][] mapEstimate_phi() {
    double[][] phi = new double[this.V][this.K];

    double numer; double denom;
    for (int w=0; w<this.V; w++) {
      for (int k=0; k<this.K; k++) {
        numer = this.CWT[w][k] + this.beta;
        denom = this.wordTerm_denomLookup[k];
        phi[w][k] = numer/denom;
      }
    }

    return phi;
  }

  void updateRunningAverages(double[][] thetaHat, double[][] phi, double[][] lambda, int nSamples) {
    // Given three new point estimates, and the number of samples we've
    // already taken, update the running averages stored in
    // this.THETA, this.THETA_dev, this.PHI_GLOBAL and this.PHI_CORPUS.
    double n = (double) nSamples;
    double oldWeight = n/(n+1);
    double newWeight = 1/(n+1);

    for (int d=0; d<this.D; d++) {
        for (int f=0; f < F; f++) {
            //this.SUPER_TRAITS[d][f] = oldWeight*this.SUPER_TRAITS[d][f] + newWeight*TRAITS[d][f];
        }
    }
    for (int k=0; k<this.K; k++) {
      for (int d=0; d<this.D; d++) {
        this.THETA[d][k] = oldWeight*this.THETA[d][k]+newWeight*thetaHat[d][k];
      }
      if( mode == Mode.TRAIN) {
          for (int f=0; f<this.F; f++ ) {
              this.LAMBDA[k][f] = oldWeight*this.LAMBDA[k][f] + newWeight*lambda[k][f];
          }
          for (int w=0; w<this.V; w++) {
            this.PHI[w][k] = oldWeight*this.PHI[w][k] + newWeight*phi[w][k];
          }
      }
    }
  }
  private double compute_dlambda(int d,int k, int f, int[] docLength) {

      return TRAITS[d][f]*alphaDK[d][k]*(Gamma.digamma(sumAlphaOverTopicK[d]) - 
                                         Gamma.digamma(sumAlphaOverTopicK[d] + docLength[d]) +  // obviously should be dev when necessary
                                         Gamma.digamma(alphaDK[d][k] + CDT[d][k]) - 
                                         Gamma.digamma(alphaDK[d][k]));

  }

  // perform one iteration of gradient ascent on lambdas
  private void do_sample_lambda_iter(double step_size) {
      //System.out.println("I am lambda:");
      //printMatrix(lambdaKF,lambdaKF.length, lambdaKF[0].length);
      //System.out.println("I am alpha:");
      //printMatrix(alphaDK,alphaDK.length, alphaDK[0].length);
      //System.out.println();

      // update the lambdas - take a gradient step
      double lambdaMean = 0.0;
      for(int k=0;k<K;k++) {
          for(int f=0;f<F;f++) {
              double docSum = 0.0;
              for(int d=0;d<D;d++) {
                  docSum += compute_dlambda(d,k,f,DOC_LENGTH);
              }
              lambdaKF[k][f] += ( docSum - (lambdaKF[k][f] / lambda_variance)) * step_size;
              lambdaMean += lambdaKF[k][f];
          }
      }
      
      lambdaMean /= K*F;

      recompute_alpha();
  }

  // perform one iteration of sampling on the train data
  abstract void do_sample_topics_iter();

  // perform one iteration of sampling on the dev data.
  // only requires parameters alpha and beta
  abstract void do_sample_topics_iter_phiFixed();

  void printMatrix(double[][] matrix, int m, int n) {
    for (int i=0; i<m; i++) {
      for (int j=0; j<n; j++) {
        System.out.print(matrix[i][j]+" ");
      }
      System.out.println();
    }
  }
}
