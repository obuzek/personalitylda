
/* Given a matrix of true outcomes and a matrix of predicted outcomes,
where each row represents a data point, return the average l2 error
per vector. */

import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;

public class Accuracy {
    // calculate l2 error between predictions and truth.
    public static double[] calculateAccuracy(double[][] truth, double[][] predicted) {
        double[] accs = new double[truth[0].length];
        RealMatrix truthMX = new Array2DRowRealMatrix(truth);
        RealMatrix predMX = new Array2DRowRealMatrix(predicted);
        int numDataPoints = truthMX.getRowDimension();
        RealMatrix diffs = truthMX.subtract(predMX);
        for (int i=0; i<truth[0].length; i++) {
            accs[i] = (diffs.getColumnVector(i)).getNorm()/numDataPoints;
        }
        return accs;
    }

    public static double[] calculateBucketAccuracy(double [][] truth, double[][] predicted) {
        double[] numTraitCorrect = new double[truth[0].length];

        for(int trait = 0; trait < truth[0].length; trait ++ ) {
            for(int doc = 0; doc < truth.length; doc ++ ) {
                double truthRound = truth[doc][trait] > 2.5 ? 5.0 : 0.0;
                if(truthRound - predicted[doc][trait] < 0.001) {
                    numTraitCorrect[trait] += 1.0;
                }
            }
            numTraitCorrect[trait] /= truth.length;
        }

        return numTraitCorrect;
    }

}
