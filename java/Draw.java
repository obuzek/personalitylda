
import java.util.Random;

public class Draw {

    static Random rdm;

    public static void main(String[] args) {
        rdm = new Random();

        double[] prob = new double[200];
        for(int i=0;i< 200; i++) {
            prob[i] += 1;
        }
        for(int i=0;i < Integer.parseInt(args[0]); i++) {
            multinom(prob);
//            System.out.print(multinom(new double[] { 0.2, 0.4, 0.3, 0.1 }) + " ");
        }
        System.out.println();
    }

  // generate from a multinomial
  static int multinom(double[] prob) {
      int result;
      double[] indexProb = new double[prob.length];
      double sum = 0.0;
      for (int i = 0; i < prob.length; i++) {
          indexProb[i] = sum;
          sum += prob[i];
      }

      double r = rdm.nextDouble() * sum;
      //binary search through index
      int beginIndex = 0;
      int lastIndex = indexProb.length; // lastIndex is exclusive
      int curr = -1; // failure condition
      while( (lastIndex - beginIndex) > 0 ) {
          curr = (beginIndex + lastIndex) / 2; // will have a slight lefthand bias
          if ( r < indexProb[curr] ) {
              lastIndex = curr; // lastIndex is exclusive
          } else if (r >= indexProb[curr] && (curr+1 == indexProb.length || r < indexProb[curr+1])) {
              return curr;
          } else { // this means r > the curr bucket, and that there is at least one bucket to the right
              beginIndex = curr+1;
          }
      }
      return curr;
  }

}
