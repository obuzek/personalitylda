
import java.io.*;
import org.apache.commons.cli.*;
import java.util.HashMap;
import java.util.ArrayList;

public class PersonalityPrediction { 

    private enum Model {
        LINLDA,
        LOGLDA,
        SILLY,
        PLAINOLDLOGREG,
        DMR
    }

    File train_file;
    File test_file;
    File features_file;
    File traits_file;
    Mode mode;
    Model model;
    double alpha = 1.0;
    double beta = 0.01;
    double lrnRate = 0.01;
    int k = 10;
    double lambda_var;
    int iters = 2000;
    int burn_iters = 1000;
    File model_file;
    boolean reportDevAcc;
    boolean scale_data;
    boolean binary_data;

    int maxVocab;

    private class Corpus {
        HashMap<Integer,Integer> docIDExtToInt = new HashMap<Integer,Integer>();
        HashMap<Integer,Integer> docIDIntToExt = new HashMap<Integer,Integer>();
        int docIDctr;
        int[][] corpus;
        double[][] traits, features;
        Mode mode;

        public Corpus(Mode mode) {
            this.docIDExtToInt = new HashMap<Integer,Integer>();
            this.docIDctr = 0;
            this.mode = mode;
        }

        public int[][] importDocs(File corpus_file) {

            BufferedReader in;
            String line;
            String[] inputs;
            ArrayList<Integer[]> alCorpus = new ArrayList<Integer[]>();
            int intDocID, extDocID, wordID, wordCount;
            try{
                in = new BufferedReader(new FileReader(corpus_file));

                while((line = in.readLine()) != null) {
                    inputs = line.split("\t");
                    extDocID = Integer.parseInt(inputs[0]);
                    wordID = Integer.parseInt(inputs[1]) - 1;
                    wordCount = Integer.parseInt(inputs[2]);

                    if (docIDExtToInt.containsKey(extDocID)) {
                        intDocID = docIDExtToInt.get(extDocID);
                    } else {
                        intDocID = docIDctr;
                        docIDctr++;
                        docIDExtToInt.put(extDocID,intDocID);
                        docIDIntToExt.put(intDocID,extDocID);
                    }

                    if (mode == Mode.TRAIN) {
                        maxVocab = Math.max(maxVocab - 1,wordID) + 1;
                    } else if (mode == Mode.TEST) {
                        if( wordID >= maxVocab) {
                            wordID = maxVocab;
                        }
                    }   

                    alCorpus.add(new Integer[] { intDocID, wordID, wordCount });

                }
            } catch (IOException ioe) {
                System.err.println(ioe.getMessage());
            }

            alCorpus.trimToSize();

            Integer[][] corparr = alCorpus.toArray(new Integer[alCorpus.size()][]);
            corpus = new int[corparr.length][];
            for(int i=0;i < corparr.length; i++) {
                corpus[i] = new int[corparr[i].length];
                for (int j=0;j < corparr[i].length; j++) {
                    corpus[i][j] = corparr[i][j].intValue();
                }
            }
            return corpus;
        }

        public double[][] importTraits(File traits_file) {
            // Makes the strong assumption that docIDs in the input corpus are 1-indexed,
            // and that a line in the traits_file has a lineID corresponding to that docID.

            BufferedReader in;
            String line;
            String[] inputs;
            double[][] corpusTraits = new double[docIDctr][]; 
            int extDocID = 0;
            try{
                in = new BufferedReader(new FileReader(traits_file));
                
                while((line = in.readLine()) != null) {
                    extDocID++;
                    if( docIDExtToInt.containsKey(extDocID) ) {
                        inputs = line.split("\t");
                        double[] featureInputs = new double[inputs.length];
                        int intDocID = docIDExtToInt.get(extDocID);
                        for (int i=0;i<inputs.length;i++) {
                            if (scale_data) 
                                if (binary_data)
                                    featureInputs[i] = Double.parseDouble(inputs[i]) / 5;
                                else
                                    featureInputs[i] = (Double.parseDouble(inputs[i]) - 1) / 4;
                            else
                                featureInputs[i] = Double.parseDouble(inputs[i]);
                        }
                        corpusTraits[intDocID] = featureInputs;
                    }

                }
            } catch (IOException ioe) {
                System.err.println(ioe.getMessage());
            }

            traits = corpusTraits;
            return corpusTraits;
        }

        public double[][] importFeatures(File feature_file) {
            // Makes the strong assumption that docIDs in the input corpus are 1-indexed,
            // and that a line in the traits_file has a lineID corresponding to that docID.
            //

            if (feature_file == null) {
                System.out.println("feature file was null!");
                return new double[corpus.length][0];
            }

            BufferedReader in;
            String line;
            String[] inputs;
            double[][] corpusFeatures = new double[docIDctr][]; 
            int extDocID = 0;
            try{
                in = new BufferedReader(new FileReader(feature_file));
                
                while((line = in.readLine()) != null) {
                    extDocID++;
                    if( docIDExtToInt.containsKey(extDocID) ) {
                        inputs = line.split("\t");
                        double[] featureInputs = new double[inputs.length];
                        int intDocID = docIDExtToInt.get(extDocID);
                        for (int i=0;i<inputs.length;i++) {
                            featureInputs[i] = Double.parseDouble(inputs[i]);
                        }
                        corpusFeatures[intDocID] = featureInputs;
                    }

                }
            } catch (IOException ioe) {
                System.err.println(ioe.getMessage());
            }

            features = corpusFeatures;
            return corpusFeatures;
        }

    }
    public static void main(String[] args) {

        PersonalityPrediction pp = new PersonalityPrediction(args);
    }

    public PersonalityPrediction(String[] args) {
        Options options = new Options();
        // training input
        options.addOption("train_file",true,"input training corpus");
        // dev input
        options.addOption("test_file",true,"input test corpus - dev for training");
        // features file - expecting line number to correspond to document ID
        options.addOption("feature_file",true,"feature file - expecting line number to correspond to docID");
        // traits file - expecting line number to correspond to document ID
        options.addOption("traits_file",true,"traits file - expecting line number to correspond to docID");
        // model
        options.addOption("model",true,"LinearLDA, LogisticLDA, SillyModel, PlainOldLogReg, DMR");
        // train or test
        options.addOption("mode",true,"train or test");
        // alpha
        options.addOption("alpha",true,"alpha value - not valid for DMR");
        // beta
        options.addOption("beta",true,"beta value");
        // scale
        options.addOption("scale",false,"do you want to scale traits?");
        // binary (traits)
        options.addOption("binary",false,"are input traits binary (essays)?");
        // learning rate.
        options.addOption("learning_rate",true,"learning rate for gradient methods. Must be non-negative.");
        // k
        options.addOption("topics",true,"num topics");
        // variance
        options.addOption("lambda_var",true,"lambda variance - only for DMR");
        // iters
        options.addOption("iters",true,"num iterations to run total");
        // burnIters
        options.addOption("burn_iters",true,"num iterations to discard for burn-in; must be < iters");
        // model output        
        options.addOption("model_file",true,"output model file for testing");

        CommandLineParser parser = new BasicParser();

        try {
            // parse the command line arguments
            CommandLine line = parser.parse( options, args );
            
            if( line.hasOption( "model" ) ) {
                // initialise the member variable
                String input = line.getOptionValue( "model" ); 
                if (input.equals("LinearLDA")) {
                    model = Model.LINLDA;
                } else if (input.equals("LogisticLDA")) {
                    model = Model.LOGLDA;
                } else if (input.equals("PlainOldLogReg")) {
                    model = Model.PLAINOLDLOGREG;
                } else if (input.equals("SillyModel")) {
                    model = Model.SILLY;
                } else if (input.equals("DMR")) {
                    model = Model.DMR;
                }
            }
            if( line.hasOption("mode") ) {
                String input = line.getOptionValue( "mode");
                if (input.equals("train")) {
                    mode = Mode.TRAIN;
                } else if (input.equals("test")) {
                    mode = Mode.TEST;
                }
            }
            if( line.hasOption("train_file") ) {
                train_file = new File( line.getOptionValue("train_file"));
            }
            if( line.hasOption("test_file") ) {
                test_file = new File( line.getOptionValue("test_file"));
                reportDevAcc = true;
            } else {
                reportDevAcc = false;
            }
            if( line.hasOption("traits_file") ) {
                traits_file = new File( line.getOptionValue("traits_file"));
            } 
            if( line.hasOption("traits_file") ) {
                traits_file = new File( line.getOptionValue("traits_file"));
            }           
            if( line.hasOption("model_file") ) {
                model_file = new File( line.getOptionValue("model_file"));
            }           
            if( line.hasOption("feature_file") ) {
                features_file = new File( line.getOptionValue("feature_file"));
            }           
            if( line.hasOption("alpha")) {
                alpha = Double.parseDouble(line.getOptionValue("alpha"));
            }
            if( line.hasOption("beta")) {
                beta = Double.parseDouble(line.getOptionValue("beta"));
            }
            if( line.hasOption("learning_rate")) {
                lrnRate = Double.parseDouble(line.getOptionValue("learning_rate"));
            }
            if( line.hasOption("lambda_var")) {
                lambda_var = Double.parseDouble(line.getOptionValue("lambda_var"));
                System.out.println(lambda_var);
            }
            if( line.hasOption("scale")) {
                scale_data = true;
            } else {
                scale_data = false;
            }
            if( line.hasOption("binary")) {
                binary_data = true;
            } else {
                binary_data = false;
            }
            if( line.hasOption("topics")) {
                k = Integer.parseInt(line.getOptionValue("topics"));
            }
            if( line.hasOption("iters")) {
                iters = Integer.parseInt(line.getOptionValue("iters"));
            }
            if( line.hasOption("burn_iters")) {
                burn_iters = Integer.parseInt(line.getOptionValue("burn_iters"));
            }

            // automatically generate the help statement
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp( "java -cp lib/commons-math3-3.2.jar:lib/commons-cli-1.2.jar:. PersonalityPrediction", options );
        }
        catch( ParseException exp ) {
            // oops, something went wrong
            System.err.println( "Parsing failed.  Reason: " + exp.getMessage() );
        }

        // CREATE MODEL OBJECT
        PersonalityModel pmo = null;
        if ( mode == Mode.TRAIN) {
            if( model == Model.LINLDA) {
                pmo = new LinearLDA(k, alpha, beta);
            } else if ( model == Model.LOGLDA) {
                pmo = new LogisticLDA(k, alpha, beta, lrnRate);
            } else if ( model == Model.SILLY) {
//                pmo = new SillyLDA(k, alpha, beta, iters, burn_iters);
            } else if ( model == Model.DMR) {
                pmo = new CollapsedDMRSampler(k, lambda_var, beta, binary_data);
            } else if ( model == Model.PLAINOLDLOGREG) {
                pmo = new PlainOldLogReg(lrnRate);
            }
            //pmo = new CollapsedDMRSampler(k, lambda_var, beta);

           // PARSE FILES
            Corpus trainCorpus = new Corpus(Mode.TRAIN);
            int[][] trainDocs = trainCorpus.importDocs(train_file);
            double[][] trainTraits = trainCorpus.importTraits(traits_file);
            double[][] trainFeatures = trainCorpus.importFeatures(features_file);
           
            // TRAIN, TEST ON TRAIN, REPORT ACCURACY
            
            pmo.train(trainDocs, trainTraits, trainFeatures,iters,burn_iters);
            double[][] trainPredictions = pmo.test(trainDocs, trainFeatures,
							iters, burn_iters);
            double[][] pred = new double[trainTraits.length][trainTraits[0].length];
            for(int i = 0; i < trainTraits.length; i++) {
                for (int j = 0; j < trainTraits[0].length; j++) {
                    if (scale_data) {
                    pred[i][j] = trainPredictions[i][j] * 5.0;
                    trainTraits[i][j] = trainTraits[i][j] * 5.0;
                    } else {
                        pred[i][j] = trainPredictions[i][j];
                    }
                }
            }
            double[] trainAccs = pmo.evaluate(trainTraits, pred);
            System.out.println("Training L2 error per user by trait:");
            for (int i=0; i<trainAccs.length; i++) {
		System.out.println( (i+1)+": "+Double.toString(trainAccs[i]));
            }
            double[] bucketAccs = Accuracy.calculateBucketAccuracy(trainTraits, trainPredictions);
            System.out.println("Bucket accuracy by trait:");
            for (int i=0; i<bucketAccs.length; i++) {
                System.out.println((i+1)+": " + Double.toString(100 * bucketAccs[i]));
            }
       
            // REPORT ACCURACY ON DEV SET IF PROVIDED
            if ( reportDevAcc ) {
                Corpus devCorpus = new Corpus(Mode.TEST);
                int[][] devDocs = devCorpus.importDocs(test_file);
                double[][] devTraits = devCorpus.importTraits(traits_file);
                double[][] devFeatures = devCorpus.importFeatures(features_file);

                double[][] devPredictions = pmo.test(devDocs, devFeatures,
							iters, burn_iters);
                pred = new double[devTraits.length][devTraits[0].length];
                for(int i = 0; i < devTraits.length; i++) {
                    for (int j = 0; j < devTraits[0].length; j++) {
                        if (scale_data) {
                        pred[i][j] = devPredictions[i][j] * 5.0;
                        devTraits[i][j] = devTraits[i][j] * 5.0;
                        } else {
                            pred[i][j] = devPredictions[i][j];
                        }
                    }
                }

                double[] devAccs = pmo.evaluate(devTraits, pred);
                System.out.println("Dev L2 error per user by trait:");
                for (int i=0; i<devAccs.length; i++) {
                    System.out.println((i+1)+": "
				+Double.toString(devAccs[i]));
                }
                bucketAccs = Accuracy.calculateBucketAccuracy(devTraits, devPredictions);
                System.out.println("Bucket accuracy by trait:");
                for (int i=0; i<bucketAccs.length; i++) {
                    System.out.println((i+1)+": " + Double.toString(100 * bucketAccs[i]));
                }
            }

            // OUTPUT MODEL FILE
            try {
                // Write to disk with FileOutputStream
                FileOutputStream f_out = new FileOutputStream("model_file");

                // Write object with ObjectOutputStream
                ObjectOutputStream obj_out = new ObjectOutputStream (f_out);

                // Write object out to disk
                obj_out.writeObject ( pmo );
            } catch (FileNotFoundException fnfe) {
                System.err.println(fnfe.getMessage());
                System.err.println("ERROR: Could not open model_file for output.  Model not saved.");
            } catch (IOException ioe) {
                System.err.println(ioe.getMessage());
                System.err.println("ERROR: Could not serialize model.  Model not saved.");
            }

        } else if ( mode == Mode.TEST) {

            Object obj = null;
            // RETRIEVE FROM MODEL FILE
            try {
                // Read from disk using FileInputStream
                FileInputStream f_in = new  FileInputStream(model_file);

                // Read object using ObjectInputStream
                ObjectInputStream obj_in = new ObjectInputStream (f_in);

                // Read an object
                obj = obj_in.readObject();
            } catch (FileNotFoundException fnfe) {
                System.err.println(fnfe.getMessage());
                System.err.println("ERROR: Could not open model_file for input.  Model not saved.");
            } catch (IOException ioe) {
                System.err.println(ioe.getMessage());
                System.err.println("ERROR: Could not serialize model.  Model not saved.");
            } catch (ClassNotFoundException cnfe) {
                System.err.println(cnfe.getMessage());
                System.err.println("ERROR: Are you sure that the model you asked for exists?");
            }

            if( model == Model.LINLDA) {
                pmo = (LinearLDA) obj;
            } else if ( model == Model.LOGLDA) {
                pmo = (LogisticLDA) obj;
            } else if ( model == Model.SILLY) {
//                pmo = (SillyLDA) obj;
            } else if ( model == Model.DMR) {
                pmo = (CollapsedDMRSampler) obj;
            } else if ( model == Model.PLAINOLDLOGREG ) {
                pmo = (PlainOldLogReg) obj;
            }
            //pmo = (CollapsedDMRSampler) obj;

            maxVocab = pmo.V;
 
            Corpus testCorpus = new Corpus(Mode.TEST);
            int[][] testDocs = testCorpus.importDocs(test_file);
            double[][] testTraits = testCorpus.importTraits(traits_file);
            double[][] testFeatures = testCorpus.importFeatures(features_file);

            double[][] testPredictions = pmo.test(testDocs, testFeatures,
							iters, burn_iters);
            double[] testAccs = pmo.evaluate(testTraits, testPredictions);
            System.out.println("Test L2 error per user by trait:");
            for (int i=0; i<testAccs.length; i++) {
                System.out.println((i+1)+": "
				+ Double.toString(testAccs[i])); 
            }

            double[] bucketAccs = Accuracy.calculateBucketAccuracy(testTraits, testPredictions);
            System.out.println("Bucket accuracy by trait:");
            for (int i=0; i<bucketAccs.length; i++) {
                System.out.println((i+1)+": " + Double.toString(100 * bucketAccs[i]));
            }
        }

    }
}
