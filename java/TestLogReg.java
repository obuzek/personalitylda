
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;

public class TestLogReg {
    public static void main(String[] args) {
        double[][] data = {{1.1,3.55,0.3},{2.0,2.6432,4.2},{1.1,1.2,-4.1}};
        double[] y = {0.4,0.2,0.77};

        RealMatrix DATA = new Array2DRowRealMatrix(data);
        RealMatrix YY = new Array2DRowRealMatrix(y);

        LogisticRegression LogReg = new LogisticRegression();
        LogReg.set_nIters(50);
        RealMatrix W = LogReg.regress(YY, DATA);

        System.out.println("If you're reading this, we performed log reg without throwing an error, which is a good start.");
    }
}
