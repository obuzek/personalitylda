import java.io.*;
import java.util.ArrayList;

class LogRegTest {
	public static void main(String[] args) {
		BufferedReader q1x, q1y;
		String line;
            	String[] inputs;
            	ArrayList<double[]> x = new ArrayList<double[]>();
		double x1,x2,y1;
		ArrayList<double[]> y = new ArrayList<double[]>();
            	try{
                	q1x = new BufferedReader(new FileReader("../q1x.dat"));
	                while((line = q1x.readLine()) != null) {
                   	     	inputs = line.split("\t");
				x1 = (double) Double.parseDouble(inputs[0]);
				x2 = (double) Double.parseDouble(inputs[1]);
				x.add(new double[] {x1,x2});
			}
			q1y = new BufferedReader(new FileReader("../q1y.dat"));
			while((line = q1y.readLine()) != null) {
                                inputs = line.split("\t");
                                y1 = (double) Double.parseDouble(inputs[0]);
                                y.add(new double[] {y1});  
                        }
		} catch (IOException ioe) {
                	System.err.println(ioe.getMessage());
            	}

		x.trimToSize();
		y.trimToSize();
		double[][] xdat = x.toArray(new double[x.size()][]);
		double[][] ydat = y.toArray(new double[y.size()][]);

		LogisticRegression logreg;
		logreg = new LogisticRegression();
		logreg.set_lrnRate(0.1);
		logreg.set_nIters(10000);
		double[][] weights = logreg.regress(ydat, xdat);
		for (int i=0; i<weights.length; i++) {
			for (int j=0; j<weights[i].length; j++) {
				System.out.println(weights[i][j]+" ");
			}
		}
	}
}


