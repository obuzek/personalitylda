import java.lang.Math;
import org.apache.commons.math3.special.Gamma;

public class DirichletGradAscent {
    /* do gradient ascent to find the alpha parameters that best explain
    a given multinomial probability distribution. See
    http://research.microsoft.com/en-us/um/people/minka/papers/dirichlet/minka-dirichlet.pdf
    for a thorough explanation. */
    private static double[] alpha;
    private static double lrnRate;
    private static double[] grad;
    private static double[] logpbar;
    private static int K; // dimensionality of alpha and grad.

    public static double[] solve(double[] Pobs) {
        return solve(Pobs, 50, 0.1);
    }

    public static double[] solve(double[] Pobs, int nIters, double lrnRate) {
        /* Pobs is an array of doubles representing an observed multinomial
        distribution. That is, Pobs is a probability over K discrete outcomes.
        We want to do gradient ascent on the dirichlet parameter vector
        alpha. */
        K = Pobs.length;
        lrnRate = 0.01;
        alpha = new double[K]; // alphas
        grad = new double[K]; // gradient of likelihood wrt alpha.
        logpbar = new double[K];

        // we care about the log probabilities, so get this.
        for (int k=0; k<K; k++) {
            logpbar[k] = Math.log(Pobs[k]);
        }

        /* d(log p(data|alpha))/d(alpha_k) = 
          = N*digamma(\sum_k alpha_k) - N*digamma(alpha_k) + N*log(\bar{p}_k)
          where
          digamma is the digamma function.
          \bar{p}_k is the k-th entry of \sum_{i=1}^N log p^{(i)}_k,
		i.e., it is the sample mean of the test statistic,
		or something to that effect.
                The above for-loop got us that, stored in this.logpbar. */
        for (int n=0; n < nIters; n++) {
            doGradientAscentIter();
        }
        return alpha;
    }

    private static void doGradientAscentIter() {

        double sumAlpha = sumVector(alpha);
        double digsum = Gamma.digamma(sumAlpha);

        for (int k=0; k<K; k++) {
            grad[k] = digsum - Gamma.digamma(alpha[k])
				+ logpbar[k];
        }

        updateAlpha();
    }

    private static double sumVector(double[] v) {
        // sum all the entries of the given array.
        double sum = 0.0;
        for (int i=0; i<v.length; i++) {
            sum += v[i];
        }
        return sum;
    }

    private static void updateAlpha() {
        /* Update this.alpha in light of the gradient and learning rate. */
        for (int k=0; k<K; k++) {
            alpha[k] = alpha[k] + lrnRate*grad[k];
        }
    }
}
