PERSONALITYLDA=/home/klevin/PGM_600.676/personalitylda
RESULTS=$PERSONALITYLDA/results
CLASSPATH=$PERSONALITYLDA/java:$CLASSPATH
LIB=$PERSONALITYLDA/java/lib

ITERS=3000
BURN=2000
TOPICS=10

TRAIN=$PERSONALITYLDA/data/essays/matlab.corpus.train
DEV=$PERSONALITYLDA/data/essays/matlab.corpus.dev
TEST=$PERSONALITYLDA/data/essays/matlab.corpus.test
TRAITS=$PERSONALITYLDA/data/essays/matlab.corpus.traits.out
WORDFTRS=$PERSONALITYLDA/data/essays/matlab.corpus.ftrs.out

#DMR=$RESULTS/DMR
#mkdir -p $DMR
#cd $DMR
#echo "Running: DMR"
#java -cp $LIB/commons-math3-3.2.jar:$LIB/commons-cli-1.2.jar:$CLASSPATH PersonalityPrediction \
#    -model DMR \
#    -train_file $TRAIN \
#    -test_file $DEV \
#    -traits_file $TRAITS \
#    -mode train \
#    -iters $ITERS \
#    -burn_iters $BURN \
#    -topics $TOPICS \
#    -lambda_var 0.5 \
#    -beta 0.01

LOGLDA=$RESULTS/LogLDA
mkdir -p $LOGLDA
cd $LOGLDA
echo "Running: LogLDA"
java -cp $LIB/commons-math3-3.2.jar:$LIB/commons-cli-1.2.jar:$CLASSPATH PersonalityPrediction \
    -model LogisticLDA \
    -train_file $TRAIN \
    -test_file $DEV \
    -traits_file $TRAITS \
    -mode train \
    -iters $ITERS \
    -burn_iters $BURN \
    -topics $TOPICS \
    -learning_rate 0.01 \
    -alpha 0.1 \
    -beta 0.01

LINLDA=$RESULTS/LinLDA
mkdir -p $LINLDA
cd $LINLDA
echo "Running: LinLDA"
java -cp $LIB/commons-math3-3.2.jar:$LIB/commons-cli-1.2.jar:$CLASSPATH PersonalityPrediction \
    -model LinearLDA \
    -train_file $TRAIN \
    -test_file $DEV \
    -traits_file $TRAITS \
    -mode train \
    -iters $ITERS \
    -burn_iters $BURN \
    -topics $TOPICS \
    -learning_rate 0.01 \
    -alpha 0.1 \
    -beta 0.01

PLAIN=$RESULTS/LogReg
mkdir -p $PLAIN
cd $PLAIN
echo "Running: PlainOldLogReg"
java -cp $LIB/commons-math3-3.2.jar:$LIB/commons-cli-1.2.jar:$CLASSPATH PersonalityPrediction \
    -model PlainOldLogReg \
    -train_file $TRAIN \
    -test_file $DEV \
    -traits_file $TRAITS \
    -feature_file $WORDFTRS \
    -mode train \
    -iters $ITERS \
    -burn_iters $BURN \
    -topics $TOPICS \
    -learning_rate 0.01

