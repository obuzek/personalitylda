
import java.io.*;
import java.util.*;

public class MCLDA_collapsed {
    static ArrayList<Integer[]> corpus;
    static CollapsedGibbsSampler gs;

    public static void main(String[] args) {
        
        if (args.length != 7) {
            System.err.println("USAGE: java MCLDA corpusFile topicsK paramAlpha paramBeta numitersT numburnTburn outfile");
            System.exit(-1);
        }

        BufferedReader in;
        String line;
        String[] inputs;
        corpus = new ArrayList<Integer[]>();
        int docID, corpID, trainOrTest, wordID, wordCount;
        try{
            in = new BufferedReader(new FileReader(args[0]));

            while((line = in.readLine()) != null) {
                inputs = line.split("\t");
                docID = Integer.parseInt(inputs[0]) - 1;
                corpID = Integer.parseInt(inputs[1]) - 1;
                trainOrTest = Integer.parseInt(inputs[2]);
                wordID = Integer.parseInt(inputs[3]) - 1;
                wordCount = Integer.parseInt(inputs[4]);

                corpus.add(new Integer[] { docID, corpID, trainOrTest, wordID, wordCount });
                
            }
        } catch (IOException ioe) {
            System.err.println(ioe.getMessage());
        }
        
        corpus.trimToSize();

        run_sampler(args);
    }

    static void run_sampler(String [] args) {
        gs = new CollapsedGibbsSampler(corpus.toArray(new Integer[corpus.size()][]),
                                Integer.parseInt(args[1]), // K - number of topics
                                           Double.parseDouble(args[2]), // alpha - parameter
                                           Double.parseDouble(args[3])); // beta - parameter
        // run the sampler.
        // T = Integer.parseInt(args[4]); NUMBER OF ITERATIONS
        // Tburn = Integer.parseInt(args[5]); NUMBER OF ITERS THAT ARE BURN-IN
        gs.run( Integer.parseInt(args[4]), Integer.parseInt(args[5]) );

        // args[6] is the name of the outfile for printing.
        gs.print_info(args[6]);
    }
}
