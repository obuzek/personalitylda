/* Class implementing generic LDA, in the hope of simplifying things a bit. */
import java.lang.Math;
import java.util.Random;
import java.util.*;

public class LDA {
    double alpha; // topic distribution hyperparameter.
    double beta; // word distribution hyperparameter.

    // Model parameters.
    int N; // number of tokens.
    int D; // number of documents.
    int K; // number of topics.
    int V; // size of vocabulary.

    // arrays for storing counts
    int[][] CDT; // D*K entries
    int[][] CWT; // V*K entries

    // store sums that we need to look up frequently.
    double[] docTerm_denomLookup;
    double[] wordTerm_denomLookup;

    // array for storing topic assignments and document information
    // TOPIC_ASSIGNMENTS is a 3*Ntrain-long array, which is actually
    // a matrix. Each row of this matrix is
    // [docID wordID z_di].
    int[][] TOPIC_ASSIGNMENTS; 

    // lets us look up how many tokens are in each document.
    int[] DOC_INFO;
    HashMap<Integer,Integer> DOCINFOHM; //we only use this for initial counting.

    // array for storing log likelihoods (for debugging).
    double[] LL;

    // arrays for storing running averages of the distributions.
    double[][] THETA; // same dimension as CDT.
    double[][] PHI; // same dimension as CWT.

    // Class for generating samples.
    ObuzekRandom obrdm;

    // CONSTRUCTOR!
    public LDA() {
        return;
    }
    public LDA(int[][] corpus, int K, double alpha, double beta) {
        this.alpha = alpha;
        this.beta = beta;
        this.K = K;
        this.obrdm = new ObuzekRandom();
        /* Each row of the corpus is
         * [docID wordID wordCount]. */
        int docID, wordID, wordCount, c;
        this.N = 0; this.D = 0; this.V = 0;
        this.DOCINFOHM = new HashMap<Integer,Integer>();
        for (int i=0; i<corpus.length; i++) {
            docID = corpus[i][0];
            wordID = corpus[i][1];
            wordCount = corpus[i][2];
            this.countCorpusRow(docID, wordID, wordCount);
        }

        /* both vocabulary and documents are 0-indexed in corpus[][].
           and we need an additional vocabulary item for OOV, so
           add 1 to the largest docID we saw, and add 2 to the largest
           word ID we saw. */
        this.V += 2;
        this.D += 1;

        /* use these counts to initialize arrays.

           first, get the information out of the HashMap
           and into this.DOC_INFO */
        this.populateDOC_INFO();

        // traverse the corpus again to initialize TOPIC_ASSIGMENTS.
        this.populateTOPIC_ASSIGNMENTS(corpus);

        /* all the other arrays can wait to be filled in, but initialize
           them now. */
        this.CDT = new int[this.D][this.K];
        this.CWT = new int[this.V][this.K];
        this.docTerm_denomLookup = new double[this.D];
        this.wordTerm_denomLookup = new double[this.K];
        this.THETA = new double[this.D][this.K];
        this.PHI = new double[this.V][this.K];
    }

    void countCorpusRow(int docID, int wordID, int wordCount) { 
        // add this information to the global counts.
        this.N += wordCount; // count new tokens.
        if (wordID > this.V) {
            this.V = wordID; // is this the highest wordID?
        }
        if (docID > this.D) {
            this.D = docID; // is this the highest docID?
        }
        if (this.DOCINFOHM.get(docID)==null) {
            this.DOCINFOHM.put(docID, wordCount);
        } else {
            this.DOCINFOHM.put(docID, this.DOCINFOHM.get(docID) + wordCount);
        }
    }

    void populateDOC_INFO() {
        this.DOC_INFO = new int[this.D];
        for (int d=0; d<this.DOC_INFO.length; d++) {
            if (this.DOCINFOHM.get(d)==null) {
                this.DOC_INFO[d] = 0;
            } else {
                this.DOC_INFO[d] = this.DOCINFOHM.get(d);
            }
        }
        this.DOCINFOHM.clear(); // don't need this information anymore.
    }

    void populateTOPIC_ASSIGNMENTS(int[][] corpus) {
        this.TOPIC_ASSIGNMENTS = new int[this.N][3]; //[docID wordID topic]
        int n = 0; //indexes into TOPIC_ASSIGMENTS.
        int w, docID, wordID, wordCount;
        for (int i=0; i<corpus.length; i++) {
            docID = corpus[i][0];
            wordID = corpus[i][1];
            wordCount = corpus[i][2];
            for (w=0; w<wordCount; w++) {
                this.TOPIC_ASSIGNMENTS[n+w][0] = docID;
                this.TOPIC_ASSIGNMENTS[n+w][1] = wordID;
                this.TOPIC_ASSIGNMENTS[n+w][2] = 0; // randomly set later.
            }
            n += wordCount;
        }
    }

    public void run(int T, int Tburn) {
        /* run T iterations of the sampling algorithm, throwing away the
	   first Tburn of them. */
        this.LL = new double[T];

        this.randomly_init_counts();

        // we're ready to sample.
        int nSamples = 0;
        // we need a place to store MAP estimates after each iteration.
        double[][] thetaHat;
        double[][] phiHat;
        for (int t=0; t<T; t++) {
            do_sample_iter();
            thetaHat = this.mapEstimate_theta();
            phiHat = this.mapEstimate_phi();
            this.LL[t] = this.computeLL(thetaHat, phiHat);
            if (t >= Tburn) { // save this sample.
                updateRunningAverages(thetaHat, phiHat, nSamples);
                nSamples++;
            } 
        }
    }

    public void run_phiFixed(int T, int Tburn) {
        /* run T iterations of the sampling algorithm, throwing away the
            first Tburn of them. */
        this.LL = new double[T];
        this.randomly_init_counts();
        // we're ready to sample.
        int nSamples = 0;
        // we need a place to store MAP estimates after each iteration.
        double[][] thetaHat;
        for (int t=0; t<T; t++) {
            do_sample_iter_phiFixed();
            thetaHat = this.mapEstimate_theta();
            // don't want to change PHI.
            this.LL[t] = this.computeLL(thetaHat, this.PHI);
            if (t >= Tburn) { // save this sample.
                updateRunningAverages(thetaHat, this.PHI, nSamples);
                nSamples++;
            }
        }
    }

    public void randomly_init_counts() {
        /* Randomly initialize all the topic assignments, and set the
           various count matrices accordingly. */

        Random rdm = new Random();

        int docID, wordID, k;
        for (int i=0; i<this.N; i++) {
            docID = this.TOPIC_ASSIGNMENTS[i][0];
            wordID = this.TOPIC_ASSIGNMENTS[i][1];
            // sample z_di as a multinomial with uniform distribution.
            this.TOPIC_ASSIGNMENTS[i][2] = rdm.nextInt(this.K);
            // update main count tables.
            k = this.TOPIC_ASSIGNMENTS[i][2];
            this.CDT[docID][k] += 1;
            this.CWT[wordID][k] += 1;
        }

        // there are a few quantities that we need on hand. Compute them now.
        
        // for every docID, I'd like to be able to sum over the topics,
        // counting how many tokens were assigned to each topic.
        for (int d=0; d<this.D; d++) {
            this.docTerm_denomLookup[d] = this.DOC_INFO[d]
						+ this.K*this.alpha;
        }
        // for every topic, I'd like to know how many words in total were
        // assigned to that topic plus smoothing term.
        for (k=0; k<this.K; k++) {
            this.wordTerm_denomLookup[k] = 0.0;
            for (int w=0; w<this.V; w++) {
                this.wordTerm_denomLookup[k] += this.CWT[w][k];
            }
            this.wordTerm_denomLookup[k] += this.V*this.beta;
        }
    }

    void do_sample_iter() {
        // run one iteration of collapsed Gibbs sampling.
        int docID, wordID, z_di;
        double docTerm, wordTerm;
        double docTerm_numer, wordTerm_numer;
        double docTerm_denom, wordTerm_denom;
        double[] zProb = new double[this.K];

        // sample one token at a time.
        for (int i=0; i<this.N; i++) {
            docID = this.TOPIC_ASSIGNMENTS[i][0];
            wordID = this.TOPIC_ASSIGNMENTS[i][1];
            // unassign the i-th token...
            this.unassign_token(i);

            // ...and resample the i-th token.
            // First, define a probability over topics...
            docTerm_denom = this.docTerm_denomLookup[docID];
            for (int k=0; k<this.K; k++) {
                docTerm_numer = this.CDT[docID][k] + this.alpha;
                wordTerm_numer = this.CWT[wordID][k] + this.beta;
                wordTerm_denom = this.wordTerm_denomLookup[k];
                docTerm = docTerm_numer/docTerm_denom;
                wordTerm = wordTerm_numer/wordTerm_denom;

                zProb[k] = docTerm*wordTerm;
            }

            // ...and now draw a single sample from it.
            z_di = this.obrdm.multinom(zProb);
            // now, update counts accordingly.
            this.assign_token(i, z_di);
        }
    }

    void do_sample_iter_phiFixed() {
        // run one iteration of collapsed Gibbs, with the given phi matrix
        // held fixed. That is, don't update word counts by topic.
        int docID, wordID, z_di;
        double docTerm, wordTerm;
        double docTerm_numer, wordTerm_numer;
        double docTerm_denom, wordTerm_denom;
        double[] zProb = new double[this.K];

        // sample one token at a time.
        for (int i=0; i<this.N; i++) {
            docID = this.TOPIC_ASSIGNMENTS[i][0];
            wordID = this.TOPIC_ASSIGNMENTS[i][1];
            if (wordID >= this.V) { // this wordID is OOV.
                wordID = this.V-1; // largest word ID is the OOV ID.
            }
            // unassign the i-th token...
            this.unassign_token(i);
            // ...and resample the i-th token.
            // First, define a probability over topics...
            docTerm_denom = this.docTerm_denomLookup[docID];
            for (int k=0; k<this.K; k++) {
                docTerm_numer = this.CDT[docID][k] + this.alpha;
                wordTerm_numer = this.PHI[wordID][k];
                // sum over all the words for any topic is one.
                wordTerm_denom = 1.0;
                wordTerm = wordTerm_numer/wordTerm_denom;
                docTerm = docTerm_numer/docTerm_denom;

                zProb[k] = wordTerm*docTerm;
            }
            // now draw a sample.
            z_di = this.obrdm.multinom(zProb);
            this.assign_token(i, z_di);
        }
    }

    double[][] mapEstimate_theta() {
        // compute the MAP estimate of the theta matrix based on the
        // current topic counts by document.
        double numer, denom;
        double[][] theta = new double[D][this.K];
        for (int d=0; d<this.D; d++) {
            for (int k=0; k<this.K; k++) {
                numer = this.CDT[d][k] + this.alpha;
                denom = this.docTerm_denomLookup[d];
                theta[d][k] = numer/denom;
            }
        }
        return theta;
    }

    double[][] mapEstimate_phi() {
        // compute the MAP estimate of the phi matrix based on the
        // current word counts by topic.
        double[][] phi = new double[this.V][this.K];
        double numer, denom;
        for (int w=0; w<this.V; w++) {
            for (int k=0; k<this.K; k++) {
                numer = this.CWT[w][k] + this.beta;
                denom = this.wordTerm_denomLookup[k];
                phi[w][k] = numer/denom;
            }
        }
        return phi;
    }

    double computeLL(double[][] thetaHat, double[][] phiHat) {
        double ll_total = 0.0; // we'll accumulate here.
        double lik_thisToken;
        int docID, wordID;
        for (int i=0; i<this.N; i++) {
            docID = this.TOPIC_ASSIGNMENTS[i][0];
            wordID = this.TOPIC_ASSIGNMENTS[i][1];
            // calculate the probability of this word under the model.
            // First, sum over all topics.
            lik_thisToken = 0.0;
            for (int z=0; z<this.K; z++) {
                double phi = phiHat[wordID][z];
                double th = thetaHat[docID][z];
                lik_thisToken += th*phi;
            }
            ll_total += Math.log(lik_thisToken);
        }
        return ll_total;
    }

    void updateRunningAverages(double[][] theta, double[][]phi, int nSamples) {
        double n = (double) nSamples;
        double oldWeight = n/(n+1);
        double newWeight = 1/(n+1);
        for (int k=0; k<this.K; k++) { 
            for (int d=0; d<this.D; d++) {
            this.THETA[d][k] = oldWeight*this.THETA[d][k]
				+ newWeight*theta[d][k];
            }
            for (int w=0; w<this.V; w++) {
                this.PHI[w][k] = oldWeight*this.PHI[w][k]
				+ newWeight*phi[w][k];
            }
        }
    }

    void unassign_token(int i) {
        // revoke the i-th token's assignment to whatever topic it had.
        int docID = this.TOPIC_ASSIGNMENTS[i][0];
        int wordID = this.TOPIC_ASSIGNMENTS[i][1];
        if (wordID >= this.V) { // OOV word ID.
            wordID = this.V-1;
        }
        int z_di = this.TOPIC_ASSIGNMENTS[i][2];

        this.CDT[docID][z_di] -= 1;
        this.docTerm_denomLookup[docID] -= 1.0;
        this.CWT[wordID][z_di] -= 1;
        this.wordTerm_denomLookup[z_di] -= 1.0;
    }

    void assign_token(int i, int k) {
        // assign the i-th token to topic k. Update counts accordingly.
        int docID = this.TOPIC_ASSIGNMENTS[i][0];
        int wordID = this.TOPIC_ASSIGNMENTS[i][1];
        if (wordID >= this.V) { // OOV word
            wordID = this.V-1;
        }
        this.TOPIC_ASSIGNMENTS[i][2] = k;
        this.CDT[docID][k] += 1;
        this.docTerm_denomLookup[docID] += 1.0;
        this.CWT[wordID][k] += 1;
        this.wordTerm_denomLookup[k] += 1.0;
    }

    double[][] getTheta() {
        return this.THETA;
    }

    double[][] getPhi() {
        return this.PHI;
    }

    void setPhi(double[][] phi) {
        this.PHI = phi;
    }

    double[] getLL() {
        return this.LL;
    }
}
