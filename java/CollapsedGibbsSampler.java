
class CollapsedGibbsSampler extends GibbsSampler {
  // CONSTRUCTOR! inherited from GibbsSampler
  public CollapsedGibbsSampler(Integer[][] corpus, int K,
                                double alpha, double beta) {
    super(corpus, K, alpha, beta);
  } 

  // perform one iteration of sampling
  void do_sample_iter() {
    int docID; int wordID; int z_di;
    double docTerm; double wordTerm;
    double docTerm_numer; double wordTerm_numer;
    double docTerm_denom; double wordTerm_denom;
    double[] zProb = new double[this.K];

    // great. Now we're ready to sample. We'll do each token
    // one at a time.
    for (int i=0; i<this.Ntrain; i++) {
      docID = this.TOPIC_ASSIGNMENTS[i][0];
      wordID = this.TOPIC_ASSIGNMENTS[i][1];
      z_di = this.TOPIC_ASSIGNMENTS[i][2];

      // unassign token i.
      // this means both updating the main count tables and the
      // temporary lookup tables that we constructed before starting this
      // for-loop.
      this.CDT[docID][z_di] -= 1;
      this.docTerm_denomLookup[docID] -= 1.0;
      this.CWT[wordID][z_di] -= 1;
      this.wordTerm_denomLookup[z_di] -= 1.0;

      ////////////////////////////////////////
      //
      // now we're ready to resample z_di.
      //
      ////////////////////////////////////////
      // first, get a probability distribution over topics.

      for (int k=0; k<this.K; k++) {
        docTerm_numer = this.CDT[docID][k] + this.alpha;
        docTerm_denom = this.docTerm_denomLookup[docID];
        wordTerm_numer = this.CWT[wordID][k] + this.beta;
        wordTerm_denom = this.wordTerm_denomLookup[k];
        docTerm = docTerm_numer/docTerm_denom;
        wordTerm = wordTerm_numer/wordTerm_denom;

        zProb[k] = docTerm*wordTerm;
      }

      // zProb now represents an unnormalized probability distribution
      // over topics for the i-th token. We're now going to draw from
      // that distribution to select a new topic assignment.
      z_di = multinom(zProb);
      this.TOPIC_ASSIGNMENTS[i][2] = z_di;

      // Reassign token i according to this new topic assignment
      this.CDT[docID][z_di] += 1;
      this.docTerm_denomLookup[docID] += 1.0;
      this.CWT[wordID][z_di] += 1;
      this.wordTerm_denomLookup[z_di] += 1.0;

    } // end resampling of token i.
  } // end iteration (one loop through all tokens)

  // perform one iteration of sampling on the test data.
  void do_sample_iter_phiFixed(double[][] phi) {
    int docID; int wordID; int z_di;
    double docTerm; double wordTerm;
    double docTerm_numer; double wordTerm_numer;
    double docTerm_denom; double wordTerm_denom;
    double[] zProb = new double[this.K];

    for (int i=0; i<this.Ntest; i++) {
      docID = this.TOPIC_ASSIGNMENTStest[i][0];
      wordID = this.TOPIC_ASSIGNMENTStest[i][1];
      z_di = this.TOPIC_ASSIGNMENTStest[i][2];

      // unassign token i.
      this.CDTtest[docID][z_di] -= 1;
      this.docTerm_denomLookup_test[docID] -= 1.0;
      // we don't have to worry about word distribution counts, because
      // this is the test set.

      // Resample z_di.
      // First, get a distribution over topics.
      docTerm_denom = this.docTerm_denomLookup_test[docID];  
      for (int k=0; k<this.K; k++) {
        docTerm_numer = this.CDTtest[docID][k] + this.alpha;
        wordTerm_numer = phi[wordID][k];
        // sum over all the words for any topic is one.
        wordTerm_denom = 1.0; 

        wordTerm = wordTerm_numer/wordTerm_denom;
        docTerm = docTerm_numer/docTerm_denom;

        zProb[k] = wordTerm*docTerm;
      }

      // now we can actually draw a sample.
      z_di = multinom(zProb);

      // update counts accordingly.
      TOPIC_ASSIGNMENTStest[i][2] = z_di;
      this.CDTtest[docID][z_di] += 1;
      this.docTerm_denomLookup_test[docID] += 1.0;
    }
  }
}
