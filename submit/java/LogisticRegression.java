import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.LUDecomposition;
import java.lang.Math;

public class LogisticRegression extends Regression {
    int nIters;
    double lrnRate;
    double LRNRATE_CONST;
    double thresh; // threshold for stopping.

    public LogisticRegression() {
        this.nIters = 10000;
        this.lrnRate = 0.1;
        this.thresh = 1E-6;
    }

    public double[][] regress_binary(double[][] y_arr, double[][] Data_arr) {
        /* Do logistic regression to find the weights W that
        maximizes the likelihood (cf Andrew Ng's notes). */
        RealMatrix y = new Array2DRowRealMatrix(y_arr);
        RealMatrix Data = new Array2DRowRealMatrix(Data_arr);

        int Xrows = Data.getRowDimension();
        int Xcols = Data.getColumnDimension();
        int Yrows = y.getRowDimension();
        int Ycols = y.getColumnDimension();

        // add a column of 1s to take care of bias term.
        RealVector ones = new ArrayRealVector(Xrows, 1.0);
        RealMatrix X = Data.createMatrix( Xrows, Xcols+1 );
        X.setColumnVector(0, ones); 
        // copy the Data matrix into X, starting in the 1-th column
        // where we're using 0-indexing.
        X.setSubMatrix(Data.getData(), 0, 1);
        
        // initialize weight vector and gradient.
        RealMatrix w = new Array2DRowRealMatrix(Xcols+1, Ycols);
        RealMatrix gradw = new Array2DRowRealMatrix(Xcols+1, Ycols);
        
        int n,i,j;
        double sig, yi, yj, xi_j, g;
        double eta = this.lrnRate;
        RealMatrix w_new = new Array2DRowRealMatrix(Xcols+1, 1);
        RealMatrix XtimesW;
        for (n=0; n<this.nIters; n++) {
            // calcualte each component of the gradient.
            XtimesW = X.multiply(w);
            for (int ycol=0; ycol<Ycols; ycol++) {
                for (j=0; j<Xcols+1; j++) {
                    gradw.setEntry(j,ycol,0.0);
                    for (i=0; i<Xrows; i++) {
                        sig = this.sigmoid(XtimesW.getEntry(i,ycol));
                        yi = y.getEntry(i,ycol);
                        xi_j = X.getEntry(i,j);
                        g = gradw.getEntry(j,ycol)
				+ (yi-sig)*xi_j/Xrows;
                        gradw.setEntry(j,ycol, g);
                    }
                }
            }
            // use grad to move toward the optimum.
            // We're trying to reach a maximum (max likelihood).
            w_new = w.add(gradw.scalarMultiply(eta));
            // if the weight vector hasn't changed much, we're done.
            if ( (w_new.subtract(w)).getFrobeniusNorm() <= this.thresh) {
                return w_new.getData();
            }
            w = w_new;
            // decrease learning rate as iterations increase.
            eta = this.lrnRate/Math.log(n+2);
        }
        return w.getData();
    }

    public double[][] regress(double[][] y_arr, double[][] Data_arr) {
        /* Do logistic regression to find the RealVector w that minimizes
        \sum (y - sigmoid(x*w))^2
        We assume that the data array has one data point per row. */
        System.out.println("I'm a LogisticRegression, regressing!");
        System.out.println(y_arr.length+" "+y_arr[0].length);
        RealMatrix y = new Array2DRowRealMatrix(y_arr);
        RealMatrix Data = new Array2DRowRealMatrix(Data_arr);
       
        int Xrows = Data.getRowDimension();
        int Xcols = Data.getColumnDimension();
        int Yrows = y.getRowDimension();
        int Ycols = y.getColumnDimension();

        // add a column of 1s to take care of bias term.
        RealVector ones = new ArrayRealVector(Xrows, 1.0);
        RealMatrix X = Data.createMatrix( Xrows, Xcols+1 );
        X.setColumnVector(0, ones); 
        // copy the Data matrix into X, starting in the 1-th column
        // where we're using 0-indexing.
        X.setSubMatrix(Data.getData(), 0, 1);

        // initialize weight vector and gradient.
        RealMatrix w = new Array2DRowRealMatrix(Xcols+1, Ycols);
        RealMatrix gradw = new Array2DRowRealMatrix(Xcols+1, Ycols);

        int n,i,j;
        double sig, yi, yj, xi_j, g;
        double eta = this.lrnRate;
        RealMatrix w_new = new Array2DRowRealMatrix(Xcols+1, 1);
        RealMatrix XtimesW;
        for (n=0; n<this.nIters; n++) {
            // calcualte each component of the gradient.
            XtimesW = X.multiply(w);
            for (int ycol=0; ycol<Ycols; ycol++) {
                for (j=0; j<Xcols+1; j++) {
                    gradw.setEntry(j,ycol,0.0);
                    for (i=0; i<Xrows; i++) {
                        sig = this.sigmoid(XtimesW.getEntry(i,ycol));
                        yi = y.getEntry(i,ycol);
                        xi_j = X.getEntry(i,j);
                        g = gradw.getEntry(j,ycol)
				+ (yi-sig)*(1-sig)*(-sig)*xi_j/Xrows;
                        gradw.setEntry(j,ycol, g);
                    }
                }
            }
            // use grad to move toward the optimum.
            w_new = w.subtract(gradw.scalarMultiply(eta));
            // if the weight vector hasn't changed much, we're done.
            if ( (w_new.subtract(w)).getFrobeniusNorm() <= this.thresh) {
                return w_new.getData();
            }
            w = w_new;
            // decrease learning rate as iterations increase.
            eta = this.lrnRate/Math.log(n+2);
        }

        return w.getData();
    }

    public double sigmoid(double z) {
        /* Return sigmoid(z) */
        return 1/(1 + Math.exp(-z));
    }
    void set_nIters(int n) {
        this.nIters = n;
    }
    void set_thresh(double t) {
        this.thresh = t;
    }
    void set_lrnRate(double l) {
        this.lrnRate = l;
        System.out.println("Set learning rate.");
    }
}
