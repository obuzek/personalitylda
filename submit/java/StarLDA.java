/* Model consists of using LDA to find topic distributions for each
 * document, and using linear regression on those topic distributions to
 * predict traits from topic distrbutions. */

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public abstract class StarLDA extends PersonalityModel {
    int K; // number of topics
    double alpha; // topic distribution hyperparameter.
    double beta; // word distribution hyperparameter.
    int T; // total number of sampling iterations to perform.
    int Tburn; // number of samples to throw away.

    LDA train_lda; // the LDA model learned on training data.
    LDA test_lda; // the LDA model for test data, where we keep phi fixed.

    double[][] lambda; // regression weights.

    // CONSTRUCTOR!
    public StarLDA() {}
    public StarLDA(int K, double alpha, double beta) {
        this.K = K;
        this.alpha = alpha;
        this.beta = beta;
    }

    public void train(int[][] trainCorpus, double[][] traits,
			double[][] features, int T, int Tburn) {
        // train using the given trainCorpus. Ignore the features.
        this.train_lda = new LDA(trainCorpus, this.K, this.alpha, this.beta);
        this.train_lda.run(T, Tburn);
        this.print2DArray(this.train_lda.getTheta(), "theta_train.mat");
        this.printPhiMatrix(this.train_lda.getPhi(), "phi.mat");
        this.printLL(this.train_lda.getLL(),"trainll.mat");

        // now do linear regression on the topics distribution against traits.
        // LDA represents theta as a DxK matrix.
        this.lambda = this.regress(traits, this.train_lda.getTheta() );
    }

    public double[][] test(int[][] testCorpus, double[][] features,
				int T, int Tburn) {
        // make predictions on the given test corpus and features.
        this.test_lda = new LDA(testCorpus, this.K, this.alpha, this.beta);
        this.test_lda.setPhi( this.train_lda.getPhi() );
        this.test_lda.run_phiFixed(T, Tburn);
       
        this.print2DArray(this.test_lda.getTheta(), "theta_test.mat"); 
        this.printLL(this.test_lda.getLL(),"testll.mat");

        // predict traits using the LDA theta matrix and the
        // regression weights we learned.
        return this.predict( test_lda.getTheta() );
    }

    public void printLL(double[] LL, String filename) {
        if (filename==null) {
            for (int i=0; i<LL.length; i++) {
                System.out.println(LL[i]);
            }
        } else {
            try {
                File f = new File(filename);
                if (!f.exists()) {
                    f.createNewFile();
                }
                FileWriter fw = new FileWriter(f.getAbsoluteFile());
                BufferedWriter bw = new BufferedWriter(fw);
                double numberToPrint; String printMe;
                for (int i=0; i<LL.length; i++) {
                    numberToPrint = LL[i];
                    printMe = String.format("%6.13e",numberToPrint);
                    bw.write(printMe+"\n");
                }
                bw.close();
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }
        }
    }

    public void printPhiMatrix(double[][] phi, String filename) {
        if (filename==null) {
            for (int i=0; i<phi.length; i++) {
                System.out.print(i+1+" "); // words ID'd from 1.
                for (int j=0; j<phi[0].length; i++) {
                    System.out.print(phi[i][j] + " ");  
                }
                System.out.println();
            }  
        } else {
            try {
                File f = new File(filename);
                if (!f.exists()) {
                    f.createNewFile();
                }
                FileWriter fw = new FileWriter(f.getAbsoluteFile());
                BufferedWriter bw = new BufferedWriter(fw);
                double numberToPrint; String printMe;
                for (int i=0; i<phi.length; i++) {
                    bw.write(i+1+" "); // words ID'd from 1.
                    for (int j=0; j<phi[0].length; j++) {
                        numberToPrint = phi[i][j];
                        printMe = String.format("%6.13e",numberToPrint);
                        bw.write(printMe+" ");
                    }
                    bw.write("\n");
                }
                bw.close();
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }
        }
    }

    // find the regression weights
    abstract double[][] regress(double[][] traits, double[][] theta);

    abstract double[][] predict(double[][] theta);
}
