
/* Runs LDA followed by linear regression on the topic ditsributions
to find the best setting of weight vectors. */

import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;

public class RunLinearLDA {
    public static void main(String[] args) {
        int K = 25;
        double ALPHA = 1.0;
        double BETA = 0.01;
        int T = 2500;
        int Tburn = 2000;
        
        // read input files into the formats that the LDA program wants.
        READ THE FILE.
        Array2DRowRealMatrix trainingTraits = READ THE FILE
        trainingTraits = trainingTraits.transpose();

        Array2DRowRealMatrix testTraits = READ THE FILE;
        testTraits = testTraits.transpose();
        READ TRAIT INFO INTO THOSE THINGS.

        // read stuff into the corpus.
        corpus = READ THE CORPUS.;

        // run LDA on the given data.
        // GibbsSampler(Integer[][] corpus, int K, double alpha, double beta)
        GibbsSampler lda = new GibbsSampler(corpus, K, ALPHA, BETA);
        lda.run(T, Tburn);

        Array2DRowRealMatrix trainingTheta, testTheta;
        trainingTheta = Array2DRowRealMatrix( lda.getThetaTrain() );
        testTheta = Array2DRowRealMatrix( lda.getThetaTest() );
        // LDA gets us a D by K matrix; we want K by D.
        trainingTheta = trainingTheta.transpose();
        testTheta = testTheta.transpose();

        // Regress the trait vectors against the topic distributions
        // for each user.
        Array2DRowRealMatrix lambda;
        LinearRegression linreg = new LinearRegression();
        lambda = linreg.regress(trainingTraits, trainingTheta);

        // use the resulting topic distributions along with the learned
        // weights to estimate trait vectors for test data.
        Array2DRowRealMatrix predictedTraits;
        predictedTraits = testTheta.multiply(lambda);

        // calculate accuracy.
        Accuracy acc = new Accuracy();
        acc.calculateAccuracy(testTraits, predictedTraits);
    }
}
