import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.QRDecomposition;

public class LinearRegression extends Regression {

    public LinearRegression() {
        super();
    }

    public double[][] regress(double[][] y_arr, double[][] Data_arr) {
        /* Do linear regression to find the RealMatrix w that minimizes
        \| y - Xw \|_2, the LSE solution. */
        Array2DRowRealMatrix y = new Array2DRowRealMatrix(y_arr);
        Array2DRowRealMatrix Data = new Array2DRowRealMatrix(Data_arr);
       
        int Xrows = Data.getRowDimension();
        // add a column of 1s to take care of bias term.
        int Xcols = Data.getColumnDimension() + 1;
        RealVector ones = new ArrayRealVector(Xrows, 1.0);

        RealMatrix X = Data.createMatrix( Xrows, Xcols );
        X.setColumnVector(0, ones); 

        // copy the Data matrix into X, starting in the 1-th column
        // where we're using 0-indexing (0-th column is all 1s for bias term).
        X.setSubMatrix(Data.getData(), 0, 1);

        RealMatrix w;
        // w = (trans(X) X)^{-1} trans(X) y
        RealMatrix Xtrans = X.transpose();
        RealMatrix XtransX = Xtrans.multiply(X);
        RealMatrix invXtransX =
		new QRDecomposition(XtransX).getSolver().getInverse();
        w = invXtransX.multiply(Xtrans);
        w = w.multiply(y);

        return w.getData();
    }

}
