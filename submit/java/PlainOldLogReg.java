import java.io.Serializable;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import java.lang.Math;

class PlainOldLogReg extends PersonalityModel {
    double[][] weights; // regression weights.
    LogisticRegression logreg;

    public PlainOldLogReg() {
    }

    public PlainOldLogReg(double lrnRate) {
        this.logreg = new LogisticRegression();
        this.logreg.set_lrnRate(lrnRate);
    }

    public void train(int[][] trainCorpus, double[][] traits,
                      double[][] features, int T, int Tburn) {
        // we throw away the train corpus and Tburn, since we aren't
        // sampling and we aren't using words.
        //
        // Scale the traits to lie between 0 and 5.
        double[][] scaledtraits = new double[traits.length][traits[0].length];
        for (int i=0; i<traits.length; i++) {
            for (int j=0; j<traits[i].length; j++) {
                scaledtraits[i][j] = traits[i][j]/5.0;
            }
        }
	// perform logistic regression.
        this.logreg = new LogisticRegression();
        this.logreg.set_nIters(T);
        this.weights = this.logreg.regress(scaledtraits, features);
        this.print2DArray(this.weights,"logreg_weights.mat");
    }

    public double[][] test(int[][] testCorpus, double[][] features,
                                        int T, int Tburn) {
        int rows = features.length;
        int cols = features[0].length;
        RealMatrix featMX = new Array2DRowRealMatrix( rows, cols+1 );
        RealVector ones = new ArrayRealVector(rows, 1.0);
        featMX.setColumnVector(0, ones);
        featMX.setSubMatrix(features, 0, 1);

        RealMatrix wtMX = new Array2DRowRealMatrix(this.weights);
        RealMatrix predTraitsMX = featMX.multiply(wtMX);
        double[][] predTraits = predTraitsMX.getData();
        int numCols = predTraits[0].length;
        System.out.println("In test(), I'm going to print this predTraits array while I traverse it.");
        for (int i=0; i<predTraits.length; i++) {
            for (int j=0; j<numCols; j++) {
                predTraits[i][j] = 5.0*this.logreg.sigmoid(predTraits[i][j]);
                System.out.println(predTraits[i][j]+" ");
            }
            System.out.println();
        }
        System.out.println("All done.");
        return predTraits;
    }

    public void setLearningRate(double lr) {
        this.logreg.set_lrnRate(lr);
    }
}
