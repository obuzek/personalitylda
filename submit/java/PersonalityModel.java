import java.io.Serializable;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public abstract class PersonalityModel implements Serializable {
    int V; // this way we can save it.  thus test processing is easier.  voila.

    // train the model using the given corpus of training instances
    // as well as their features (e.g., network characteristics) and
    // true trait labels.
    public abstract void train(int[][] trainCorpus, double[][] traits,
					double[][] features, int T, int Tburn);

    // test the model on the given test corpus and features.
    // return the predicted traits in an array.
    public abstract double[][] test(int[][] testCorpus, double[][] features,
					int T, int Tburn);

    // evaluate the model's accuracy.
    public double[] evaluate(double[][] trueTraits, double[][] guessTraits) {
        this.print2DArray(trueTraits, null);
        System.out.println("=======================================");
        this.print2DArray(guessTraits, null);
        return Accuracy.calculateAccuracy(trueTraits, guessTraits);
    }

    public void print2DArray(double[][] arr, String filename) {
        if (filename==null) {
            for (int i=0; i<arr.length; i++) {
                for (int j=0; j<arr[0].length; j++) {
                    System.out.print(arr[i][j] + " ");
                }
                System.out.println();
            }
        } else {
            try {
                File f = new File(filename);
                if (!f.exists()) {
                    f.createNewFile();
                }
                FileWriter fw = new FileWriter(f.getAbsoluteFile());
                BufferedWriter bw = new BufferedWriter(fw);
                double numberToPrint; String printMe;
                for (int i=0; i<arr.length; i++) {
                    for (int j=0; j<arr[0].length; j++) {
                        numberToPrint = arr[i][j];
                        printMe = String.format("%6.13e",numberToPrint);
                        bw.write(printMe+" ");
                    }
                    bw.write("\n");
                }
                bw.close();
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }
        }
    }  
}
