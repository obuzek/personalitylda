import java.io.Serializable;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.linear.RealMatrix;

public abstract class Regression implements Serializable {

    public Regression() {
        return;
    }

    /* Given a set of outputs and a set of data inputs, return the
    vector of weights that minimizes the objective function */
    abstract double[][] regress(double[][] Observations, double[][] Data);

}
