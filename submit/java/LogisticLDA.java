import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import java.lang.Math;

public class LogisticLDA extends StarLDA {
    double[][] weights;
    LogisticRegression logreg;

    public LogisticLDA(int K, double alpha, double beta, double lrnRate) {
        super(K, alpha, beta);
        this.logreg = new LogisticRegression();
        this.logreg.set_lrnRate(lrnRate);
    } 

    public LogisticLDA() {} // this exists to do regress

    public double[][] regress(double[][] traits, double[][] theta) {
        this.logreg = new LogisticRegression();
        // scale all traits to lie between 0 and 1.
        int numCols = traits[0].length;
        double[][] scaledtraits = new double[traits.length][traits[0].length];
        for (int i=0; i<traits.length; i++) {
            for (int j=0; j<numCols; j++) {
                scaledtraits[i][j] = traits[i][j]/5.0;
            }
        }
        this.weights = this.logreg.regress(scaledtraits, theta);
        this.print2DArray(this.weights, "logreg_weights.mat");
        return this.weights;
    }

    public double[][] predict(double[][] theta) {
        // predict a matrix of traits based on this theta matrix and
        // our learned regression weights.
        int rows = theta.length;
        int cols = theta[0].length;
        // add an extra column for the all-ones...
        RealMatrix thetaMX = new Array2DRowRealMatrix( rows, cols+1 );
        // ...and fill that first column with 1s.
        RealVector ones = new ArrayRealVector(rows, 1.0);
        thetaMX.setColumnVector(0, ones);
        // set the rest of the matrix to contain the theta data.
        thetaMX.setSubMatrix(theta, 0, 1);

        RealMatrix lamMX = new Array2DRowRealMatrix(this.lambda);
        RealMatrix predTraitsMX = thetaMX.multiply(lamMX);
        double[][] predTraits = predTraitsMX.getData();
        int numCols = predTraits[0].length;
        for (int i=0; i<predTraits.length; i++) {
            for (int j=0; j<numCols; j++) {
                // our model predicts in logistic space, and we have to
                // rescale so that our predictions are on the 0 to 5 scale
                // that we started with.
                predTraits[i][j] = 5.0*sigmoid(predTraits[i][j]);
            }
        }
        return predTraits;
    }

    public void setLearningRate(double lr) {
        this.logreg.set_lrnRate(lr);
    }

    public double sigmoid(double z) {
        return 1/(1 + Math.exp(-z));
    }

}
