import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;

public class LinearLDA extends StarLDA {
    public LinearLDA(int K, double alpha, double beta) {
        super(K, alpha, beta);
    }

    public double[][] regress(double[][] traits, double[][] theta) {
        LinearRegression linreg = new LinearRegression();
        return linreg.regress(traits, theta);
    }

    public double[][] predict(double[][] theta) {
        // predict a matrix of traits based on this theta matrix and
        // our learned regression weights.
        RealMatrix thetaMX = new Array2DRowRealMatrix(theta.length,
						theta[0].length+1);
        thetaMX.setColumnVector(0, new ArrayRealVector(theta.length, 1.0));
        thetaMX.setSubMatrix(theta, 0, 1);

        RealMatrix lamMX = new Array2DRowRealMatrix(this.lambda);
        RealMatrix predTraitsMX = thetaMX.multiply(lamMX);
        double[][] predTraits = predTraitsMX.getData();
        int numCols = predTraits[0].length;
        for (int i=0; i<predTraits.length; i++) {
            for (int j=0; j<numCols; j++) {
                if (predTraits[i][j] > 5.0) {
                    predTraits[i][j] = 5.0;
                }
                if (predTraits[i][j] < 0.0) {
                    predTraits[i][j] = 0.0;
                }
            }
        }
        return predTraits;
    }

}
