
/* Class for running Olivia's random sampling code. */

import java.util.Random;

public class ObuzekRandom {
    static Random rdm = new Random(); // RNG

    static int sampleBinom(double lambda) {
        double r = rdm.nextDouble();

        if ( r < lambda ) {
            return 1;
        } else {
            return 0;
        }
    }

    // generate from a multinomial
    static int multinom(double[] prob) {
        double[] indexProb = new double[prob.length];
        double sum = 0.0;
        for (int i = 0; i < prob.length; i++) {
            indexProb[i] = sum;
            sum += prob[i];
        }

        double r = rdm.nextDouble() * sum;
        //binary search through index
        int beginIndex = 0;
        int lastIndex = indexProb.length; // lastIndex is exclusive
        int curr = -1; // failure condition

        while( (lastIndex - beginIndex) > 0 ) {
            // will have a slight lefthand bias
            curr = (beginIndex + lastIndex) / 2;
            if ( r < indexProb[curr] ) {
                lastIndex = curr; // lastIndex is exclusive
            } else if ( r >= indexProb[curr]
			&& (curr+1 == indexProb.length
			|| r < indexProb[curr+1]) ) {
              return curr;
            } else { // this means r > the curr bucket, and that
              // there is at least one bucket to the right
              beginIndex = curr+1;
            }
        }
        return curr;
    }
}
