#!/usr/bin/env python

import sys, csv, argparse, os, re
from collections import defaultdict, Counter, namedtuple

parser = argparse.ArgumentParser()

parser.add_argument("suchi_train_corpus",help="input_train.txt",
                    type=str)
parser.add_argument("suchi_test_corpus",help="input_test.txt",
                    type=str)
parser.add_argument("-wd","--workingd",help="working directory to place output files",
                    type=str,
                    default=".",
                    dest="work")
parser.add_argument("-v","--vocab",help="vocab output file",
                    type=str,
                    default="vocab.out",
                    dest="vocab")
parser.add_argument("-m","--matlab",help="file to output matlab-formatted corpus to",
                    type=str,
                    default="matlab.corpus.out",
                    dest="matcorp")


args = parser.parse_args()

wd = os.path.abspath(args.work)
if not os.path.isdir(wd):
    os.mkdir(wd)

f = lambda line: re.match("(\d) (\w+( \w+)*)",line).groups()[:2]
train_corpus = open(args.suchi_train_corpus)
test_corpus = open(args.suchi_test_corpus)

class Vocab:

    def __init__(self):
        self.vocabfreq = Counter()
        self.vocab = {}
        self.vocab_revindex = {}
        self.index_stale = True
        self.num_vocab = 0

    def __iter__(self):
        return self.vocab.__iter__()

    def observe(self,token):
        if token not in self.vocab:
            self.num_vocab += 1
            self.vocab[token] = self.num_vocab
            self.index_stale = True

        self.vocabfreq[self.get_index(token)] += 1

        return self.get_index(token)

    def get_token(self,idx):
        if self.index_stale:
            self.update_index()
        return self.vocab_revindex[idx]
    
    def get_index(self,token):
        return self.vocab[token]

    def get_count(self,token):
        return self.vocabfreq[self.vocab[token]]

    def update_index(self):
        self.vocab_revindex = dict((idx,vocab) for (vocab,idx) in self.vocab.iteritems())
        self.index_stale = False

V = Vocab()

corpus_out = open(args.work+"/"+args.matcorp,"w+")
train_docs = defaultdict(dict)
num_docs = Counter()
test_docs = defaultdict(dict)

def print_corpus(corpus,mrkr,out_file):
    for line in corpus:
        corpus_id, doc = f(line)
        corpus_id = int(corpus_id) + 1 # offset by 1 for matlab convenience
        tokens = [V.observe(s) for s in doc.split(" ")]

        num_docs[mrkr] += 1
        doc_id = num_docs[mrkr] # I think, at least

        tc = Counter(tokens)
        for tkn in tc:
            out_file.write("%d\t%d\t%d\t%d\t%d\n" % (doc_id,corpus_id,mrkr,tkn,tc[tkn]))
        out_file.flush()

print_corpus(train_corpus,0,corpus_out)
print_corpus(test_corpus,1,corpus_out)

corpus_out.close()

vocab_out = open(args.work+"/"+args.vocab,"w+")

V.update_index()
for idx in sorted(V.vocab_revindex.keys()):
    vocab = V.get_token(idx)
    vocab_count = V.get_count(vocab)
    vocab_out.write("%d\t%d\t%s\n" % (idx,vocab_count,vocab))
    vocab_out.flush()

vocab_out.close()
