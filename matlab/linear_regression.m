function [LAM] = linear_regression(TH, TRAITMX)
  % [LAM] = linear_regression(TH, TRAITMX)
  %
  % Find the matrix of weights LAM that minimizes the OLS error for
  % TH*LAM = TRAITMX
  % Thus, each row of TH is a set of observations.
  % Each column of LAM is a set of weights.
  % Each row of TRAITMX is a set of observations.

  LAM = inv(TH' * TH)* TH' * TRAITMX;

end%function
