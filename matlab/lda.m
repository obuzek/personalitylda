function [TH, PH] = lda(corpus, alpha, beta, T, Tburn)
  % LDA( CORPUS, ALPHA, BETA, T, Tburn )
  %
  % CORPUS is a matrix representing a set of documents. Each document is
  %	represented as a bag of words. Each document has a unique ID from
  %	1 to D, where D is the number of documents. Each word has a unique ID
  %	from 1 to W where W is the number of word types. Each row of CORPUS
  %	consists of three numbers, [d w c] where d is a document ID, w is a
  %	word ID, and c is the number of times the word with ID w appeared in
  % 	document d. Words not accounted for by a row for a given document are
  %	considered to have count 0 in that document. So if, for example, there
  %	is no row [5 3 X] for any X, it is assumed that document 5 had no
  %	instances of word 3.
  % ALPHA is the vector for the Dirichlet prior over topic distributions.
  %	Thus alpha has as many components as there are topics.
  % BETA is a vector for the Dirichlet prior over word distributions. The same
  %	prior over word distributions is shared among all topics. Beta has
  %	as many components as there are words.
  % T is the number of Gibbs sampling rounds to perform before returning
  %	our estimates.
  % Tburn tells us how many of those rounds to throw out.

  if Tburn >= T
    error(['Burn-in time Tburn must be less than total iterations T.']);
  end%if

  [nrows, ~] = size(corpus);

  K = length(alpha);
  W = length(beta);
  % assumption: document IDs start at 1 and ascend, without skipping any IDs.
  D = max(corpus(:,1));

  [CWT, CDT, topicAssignments] = lda_init_counts(corpus, W, K, D);

  [nTokens, ~] = size(topicAssignments);

  % TH and PH will be a running average of our samples.
  TH = zeros(D,K);
  PH = zeros(K,W);

  % perform the Gibbs sampling.
  numSamples = 0; % tracks how many samples we've saved.
  for t = 1:T
    [theta,phi,CWT,CDT,topicAssignments] = lda_do_sample_round(CWT,CDT,topicAssignments,alpha,beta);
    % if the burn-in period has passed, keep these samples.
    if t>Tburn
      TH = updateRunningAverage(TH, theta, numSamples);
      PH = updateRunningAverage(PH, phi, numSamples);
      numSamples = numSamples + 1;
    end%if
  end%for

end%function
