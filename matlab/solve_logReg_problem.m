function [lam] = solve_logReg_problem(TRAITS, TH, nIters, lrnRate)
  % lam = solve_logReg_problem(TRAITS, TH, nIters);
  %
  % Solve the logistic regression problem of
  % min \sum_i (TRAITS(i) - sigmoid(lam' * TH(i,:)' ))

  [U,K] = size(TH);
  U = length(TRAITS)
  lam = zeros(K,1);

  gradlam = zeros(size(lam));
  thresh = 1e-8; % change less than this and we assume we've converged.

  for n = 1:nIters
    for j=1:K
      gradlam(j)=0; % reset the gradient before we accumulate into it.
      for i=1:U
        sig = sigmoid( TH(i,:)*lam);
        % derivative of sig(z) is sig(z)*(1-sig(z)), which is pretty cool.
        gradlam(j) = gradlam(j) + (TRAITS(i)-sig)*(1-sig)*(-sig)*(TRAITS(j));
      end%for
      % use the gradient to move towards the optimum. We're trying to minimize,
      % so move in the NEGATIVE direction of the gradient.
      lam_new = lam + lrnRate*gradlam;
      if norm(lam_new-lam) <= thresh
        lam = lam_new;
        return;
      end%if
    end%for
  end%for

end%function





