[theta,CDT] = fixedPhi_do_sample_round(CDT,PH,topicAssignments,beta)
  % [theta,CDT] = testTime_doSampleRound(CDT,PH,topicAssignments,beta)
  %
  % Perform LDA sampling, but where word distribution by topics remains
  % fixed. That is, we resample assignments of words to topics, but we do
  % not resample the word distributions by topic. The goal is to find the
  % best assignment of the words given in count matrix CDT to topics, with
  % PH giving our existing known word distributions by topic.

  [nTokens, ~] = size(topicAssignments);
  [K, V] = size(PH);
  [D, K] = size(CDT);

  for i=1:nTokens
    % resample the i-th topic assignment.
    docID = topicAssignments(i, 1);
    wordID = topicAssignments(i, 2);
    topic = topicAssignments(i, 3);
    % reassign one instance of word wordID in document docID
    % first, decrement counts.
    CDT( docID, topic) = CDT( docID, topic) - 1;

    % now, calculate the distribution over topics for the i-th word,
    % conditioned on everything else.
    zProb = zeros(1,K);
    for j=1:K
      zProb(j) = CDT(docID,j)*PH(j,wordID);
    end%for
    zProb = zProb/sum(zProb);
    % draw from this distribution.
    newTopic = find( mnrnd(1, zProb) );

    % update accordingly.
    CDT( docID, newTopic) = CDT( docID, newTopic) + 1;
    topicAssignments(i, 3) = newTopic;  
  end%for

end%function
