function [theta, phi] = calculate_final_matrices(CWT, CDT, alpha, beta)
  [W, T] = size(CWT);
  [D, ~] = size(CDT);

  theta = zeros(D, T);
  phi = zeros(T, W);

  % fill in each of these two matrices.
  alphaSum = sum(alpha);
  betaSum = sum(beta);
  for t = 1:T
    for wID = 1:W
      phi(t,wID) = (CWT(wID,t) + beta(wID))/(sum(CWT(:,t)) + betaSum);
    end%for
    for dID = 1:D
      theta(dID,t) = (CDT(dID,t) + alpha(t))/(sum(CDT(dID,:)) + alphaSum);
    end%for
  end%for

  % each row of theta is supposed to be a prob. dist.
  for i = 1:D
    theta(i, :) = theta(i,:)./sum(theta(i,:));
  end%for
  % similarly for phi. Each row is a distribution over words.
  for i = 1:T
    phi(i, :) = phi(i,:)./sum(phi(i,:));
  end%for
end%function
