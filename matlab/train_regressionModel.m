function [TH,PH,LAM] = train_regressionModel(CORPUS,TRAITMX,nTraits,alpha,beta,T,Tburn,regression_type)
  % [TH,PH,LAM] = train_regressionModel(CORPUS,TRAITMX,K,nTraits,alpha,beta,T,Tburn,regression_type)
  %
  % Run LDA on the given corpus with K topics and hyperparameters alpha and
  % beta (with K determined by the length of vector alpha),
  % with T iterations, with the first Tburn being treated as burn-in.
  % nTraits is the number of traits that each user has. This information is
  % stored in TRAITMX, with each row having nTraits entries. We assume one
  % row for each user, with the u-th row corresponding to the user with
  % ID u.
  % We assume that user IDs are assigned consecutively starting at 1.
  % Once we run LDA and have our estimates of the TH and PH matrices, we
  % use the TH matrix to estimate LAM, the weights that relate each user's
  % distribution of topics to their personality traits. LAM is determined
  % either through linear or logistic regression, depending on whether
  % regression_type=='linear' or regression_type=='logistic'.

  [TH, PH] = lda(CORPUS, alpha, beta, T, Tburn);

  K = length(alpha);
  LAM = zeros(K, nTraits);

  % figure out best setting of LAM based on TH and PH.
  if regression_type=='linear'
    % Do linear regression with OLS
    LAM = linear_regression(TH, TRAITMX);
  elseif regression_type=='logistic'
    % do logistic regression
    LAM = logistic_regression(TH, TRAITMX);
  else
    error(['Unrecognized regression option: ' regression_type]);
  end%if

end%function 
