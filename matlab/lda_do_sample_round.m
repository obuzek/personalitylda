[theta,phi,CWT,CDT,topicAssignments] = lda_do_sample_round(CWT,CDT,topicAssignments,alpha,beta);
  % Perform one round of resampling of all the words in the corpus.

  [nTokens, ~] = size(topicAssignments);
  [V, K] = size(CWT);
  [D, K] = size(CDT);

  for i = 1:nTokens
    docID = topicAssignments(i, 1);
    wordID = topicAssignments(i, 2);
    topic = topicAssignments(i, 3);
    % reassign one instance of word wordID in document docID
    % first, decrement counts.
    CWT( wordID, topic) = CWT( wordID, topic) - 1;
    CDT( docID, topic) = CDT( docID, topic) - 1;
    % choose a new topic to assign to, and update accordingly.
    topicDist = generateTopicDistribution(CWT,CDT,alpha,beta,wordID,docID);
    newTopic = find( mnrnd(1, topicDist) );
    CWT(wordID, newTopic) = CWT(wordID, newTopic) + 1;
    CDT( docID, newTopic) = CDT( docID, newTopic) + 1;
    topicAssignments(i, 3) = newTopic;
  end%for

  [theta, phi] = calculate_final_matrices(CWT, CDT, alpha, beta);

end%function
