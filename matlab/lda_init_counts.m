[CWT, CDT, topicAssignments] = lda_init_counts(corpus, W, K, D)
  % 

  CWT = zeros(W,K);
  CDT = zeros(D,K);
  % each row of topic assignments corresponds to a specific word token.
  % each such row has the form [docID wordID topicID]
  topicAssignments = zeros( sum( corpus(:,3) ), 3); 

  % initialize CWT, CDT and topicAssignments.
  j = 1; % indexes rows of topicAssignments
  for i = 1:nrows
    docID = corpus(i, 1);
    wordID = corpus(i, 2);
    count = corpus(i, 3);
    for c = 1:count
      % assign each instance of the word to a topic.
      topic = randi(K, 1);
      CWT( wordID, topic) = CWT( wordID, topic) + 1;
      CDT( docID, topic) = CDT( docID, topic) + 1;
      topicAssignments(j, :) = [docID wordID topic];
      j = j + 1;
    end%for
  end%for

end%function
