function [TRAITMX] = test_regressionModel(CORPUS,PH,LAM,alpha,beta,T,Tburn,regression_type)
  % [TRAITMX] = test_regressionModel(CORPUS,PH,LAM,alpha,beta,T,Tburn,regression_type)

  [nrows, ~] = size(CORPUS);

  K = length(alpha);
  W = length(beta);
  % assumption: document IDs start at 1 and ascend, without skipping any IDs.
  D = max(corpus(:,1));

  [CWT, CDT, topicAssignments] = lda_init_counts(CORPUS, W, K, D);

  [nTokens, ~] = size(topicAssignments);

  % TH will be a running average of our samples.
  TH = zeros(D,K);

  % perform the Gibbs sampling.
  % TH is fixed, so we only reassign topics. We do not change counts in
  % CWT, because we are keeping PHI fixed.
  numSamples = 0; % tracks how many samples we've saved.
  for t = 1:T
    [theta,CDT] = fixedPhi_do_sample_round(CDT,PH,topicAssignments,beta);
    % if the burn-in period has passed, keep these samples.
    if t>Tburn
      TH = updateRunningAverage(TH, theta, numSamples);
      numSamples = numSamples + 1;
    end%if 
  end%for

  % now we have an estimate of the THETA matrix for the test corpus.
  % Now we predict traits based on this THETA matrix using our
  % weights LAMBDA.


end%function
