function [LAM] = logistic_regression(TH, TRAITMX)
  % LAM = logistic_regression(TH, TRAITMX)
  %
  % TRAITMX is a matrix where the u-th row is [trait1 ... traitN]
  %	for the u-th user.
  % TH is a D by K matrix, where the d-th row is the distribution over topics
  %	of the d-th document.
  %
  % For each of the traits t, we want to learn a weight vector LAM_t
  %	that minimizes \sum_i ( TRAITMX(u,i) - sigmoid(LAM_T'*TH(u,:)) )^2.

  [D,T] = size(TRAITMX);
  [~,K] = size(TH);

  LAM = zeros(K, T);

  nIters = 50; % upper bound on # of iterations to do in grad descent.
  lrnRate = .1; % learning rate for gradient descent.

  % We have T different minimization problems to solve.
  for t=1:T
    LAM(:,t) = solve_logReg_problem(TRAITMX(:,t), TH, nIters, lrnRate);
  end%for

end%for
