function topicDist = generateTopicDistribution(CWT, CDT, alpha, beta, wID, dID)
  % generateTopicDistribution(CWT, CDT)
  % CWT is a matrix whose (wordID,topicID) entry is the number of times
  %     word wordID has been drawn from topic topicID.
  % CDT is a matrix whose (docID, topicID) entry is the number of words in
  %     document docID that were drawn from topicID.
  % alpha and beta are Dirichlet prior vectors.
  %
  % Use all this to generate a vector whose t-th entry is the
  % probability of a word wID in document dID having topic t.

  T = length(alpha);

  topicDist = zeros(1, T);

  for t = 1:T
    % note: I'm writing "topicGivenX", which is kind of misleading here.
    % the two quantities we calculate are smoothed counts, and they are
    % strictly from within a single document.
    topicGivenWord = (CWT(wID, t) + beta(wID))/(sum(CWT(:,t)) + sum(beta));
    topicGivenDoc = (CDT(dID, t) + alpha(t))/(sum(CDT(dID,:)) + sum(alpha));

    topicDist(t) = topicGivenWord*topicGivenDoc;
  end%for

  topicDist = topicDist./sum(topicDist); % normalize so it's a prob. measure
end%function
