function [newMean] = updateRunningAverage(oldMean, newSample, numPrevSamples)
  % [newMean] = updateRunningAverage(oldMean, newSample, numPrevSamples)
  %
  % Update a running average based on a newly received sample.
  %
  % oldMean is the average prior to receiving sample newSample.
  % numPrevSamples is the number of samples that oldMean is based on.
  % Thus, newSample is the (numPrevSamples+1)-th sample.
  % newSample is a new sample.
  %
  % Return the updated running average.
  %
  % oldMean and newSample can be of any dimension, so long as they agree.

  n = numPrevSamples;
  newMean = (n*oldMean)./(n+1) + newSample./(n+1);

end%function
