#!/bin/bash

# UNCOMMENT FOR TWITTER DATA
java -cp jerboa.jar -DTwitterTokenizer.unicode=jerboa/proj/tokenize/unicode.csv edu.jhu.jerboa.processing.TwitterTokenizer < $1

# UNCOMMENT FOR ESSAYS DATA
# java -cp stanford-core-nlp-1.3.5.jar edu.stanford.nlp.process.PTBTokenizer -preserveLines < $1
